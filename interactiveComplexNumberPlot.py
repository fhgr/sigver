# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 14:11:57 2021

InteractiveComplexNumberPlot.py
  Öffnet ein Fenster zur graphischen Berechnung von Addition und
  Multiplikation zweier komplexer Zahlen.
  
  Das Tool erleichtert die Umrechnung zwischen Polar- und kartesischen
  Koordinaten, und visualisiert die Eingangsgrössen und das Ergebnis
  in der komplexen (Gauss'schen) Zahlenebene
                    
https://learndataanalysis.org/how-to-update-a-matplotlib-graph-in-a-pyqt5-application-pyqt5-tutorial/
https://zetcode.com/pyqt/qlineedit/
DockPanel: https://www.youtube.com/watch?v=Y6_vNgSyQtU
@author: birkudo
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QDockWidget, QWidget, QLineEdit, QHBoxLayout, QVBoxLayout, QGridLayout, QPushButton, QLabel
from PyQt5.QtGui import QColor, QFont
from PyQt5 import QtCore
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib
from scipy import signal
import numpy as np
from numpy import *
import seaborn as sns
import os


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Complex Number Inspector UB')
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)

        # self.layout = QVBoxLayout()
        # self.setLayout(self.layout)

        self.plotStyle = 'bmh'

        color  = QColor(255, 32, 32)
        alpha  = 140
        self.errorColor = "{r}, {g}, {b}, {a}".format(r = color.red(),
                                             g = color.green(),
                                             b = color.blue(),
                                             a = alpha
                                             )

        GUIlayout = self.init_GUI_panel()
        self.dock = QWidget()
        self.dock.setLayout(GUIlayout)

        self.canvas = FigureCanvas(plt.Figure(figsize=(15, 6)))
        self.setCentralWidget(self.canvas)
        
        self.dockWidget=QDockWidget('Dock',self)        
        self.dockWidget.setFloating(False)
        self.dockWidget.setWidget(self.dock)
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.dockWidget)

        self.insert_ax()
        self.update_chart()
        

    def init_GUI_panel(self):
        # Create a QFormLayout instance
        layout = QGridLayout()

        # Add widgets to the layout
        layout.addWidget(QLabel("Real"), 0, 2)
        layout.addWidget(QLabel("Imag"), 0, 3)
        layout.addWidget(QLabel("Radius"), 0, 4)
        layout.addWidget(QLabel("Angle"), 0, 5)
        
        self.input_z1 = QLineEdit('1+i')
        self.input_z1.setToolTip('enter complex number z1') 
        layout.addWidget(QLabel("z1:"), 1, 0)
        layout.addWidget(self.input_z1, 1, 1)

        self.input_z2 = QLineEdit('0')
        self.input_z2.setToolTip('enter complex number z2') 
        layout.addWidget(QLabel("z2:"), 2, 0)
        layout.addWidget(self.input_z2, 2, 1)

        self.inputFcn = QLineEdit('z1+z2')
        self.inputFcn.setToolTip('enter definition of the resulting complex number') 
        layout.addWidget(QLabel("function:"), 3, 0)
        layout.addWidget(self.inputFcn, 3, 1)

        self.drawButton = QPushButton("Draw")
        self.drawButton.setToolTip('update/redraw plot') 
        self.drawButton.clicked.connect(self.update_chart)
        layout.addWidget(self.drawButton,4, 0)

        self.errorMsg = QLabel('')
        layout.addWidget(self.errorMsg,4, 1)

        layout.setColumnStretch(0, 1)
        layout.setColumnStretch(1, 3)
        layout.setColumnStretch(2, 1)
        layout.setColumnStretch(3, 1)
        layout.setColumnStretch(4, 1)
        layout.setColumnStretch(5, 1)
        # self.gridLayout.setRowStretch(0, 3)
        # self.gridLayout.setRowStretch(1, 1)
    
        self.input_z1.returnPressed.connect(self.update_chart)
        self.input_z2.returnPressed.connect(self.update_chart)
        self.inputFcn.returnPressed.connect(self.update_chart)

        self.z1_real   = QLabel(""); layout.addWidget(self.z1_real,   1, 2)
        self.z1_imag   = QLabel(""); layout.addWidget(self.z1_imag,   1, 3)
        self.z1_radius = QLabel(""); layout.addWidget(self.z1_radius, 1, 4)
        self.z1_angle  = QLabel(""); layout.addWidget(self.z1_angle,  1, 5)

        self.z2_real   = QLabel(""); layout.addWidget(self.z2_real,   2, 2)
        self.z2_imag   = QLabel(""); layout.addWidget(self.z2_imag,   2, 3)
        self.z2_radius = QLabel(""); layout.addWidget(self.z2_radius, 2, 4)
        self.z2_angle  = QLabel(""); layout.addWidget(self.z2_angle,  2, 5)

        self.z_real   = QLabel(""); layout.addWidget(self.z_real,   3, 2)
        self.z_imag   = QLabel(""); layout.addWidget(self.z_imag,   3, 3)
        self.z_radius = QLabel(""); layout.addWidget(self.z_radius, 3, 4)
        self.z_angle  = QLabel(""); layout.addWidget(self.z_angle,  3, 5)

        font = self.z1_angle.font()
        font.setFamily('Times')
        self.z1_real.setFont(font)
        self.z1_imag.setFont(font)
        self.z1_radius.setFont(font)
        self.z1_angle.setFont(font)

        self.z2_real.setFont(font)
        self.z2_imag.setFont(font)
        self.z2_radius.setFont(font)
        self.z2_angle.setFont(font)

        self.z_real.setFont(font)
        self.z_imag.setFont(font)
        self.z_radius.setFont(font)
        self.z_angle.setFont(font)
        
        return(layout)        
        
    def insert_ax(self):
        # new font settings
        font = {
            'weight': 'normal',
            'size': 16
        }
        # matplotlib.rc('font', **font)

        with plt.style.context(self.plotStyle):
            self.ax = self.canvas.figure.subplots()
            self.ax.set_position([0.125, 0.13, 0.775, 0.77])
            for item in ([self.ax.title, self.ax.xaxis.label, self.ax.yaxis.label] +
                    self.ax.get_xticklabels() + self.ax.get_yticklabels()):
                item.set_fontsize(font['size'])        
                item.set_fontweight(font['weight'])
        # plt.style.use('fivethirtyeight')

    # def getValue(self, inputField, defaultValue = 1.0):
    #     value = inputField.text()
    #     i = 0+1j
    #     j = i
    #     try:
    #         value = np.array(eval(value))
    #     except ValueError:
    #         value = defaultValue
    #     return(value)

    def get_result(self):
        
        i = 0+1j
        j = i

        fcn  = self.inputFcn.text()
        z1   = self.input_z1.text()
        z2   = self.input_z2.text()

        error = None
        # value = eval(fcn)
        try:
            z1    = np.array(eval(z1))
            z2    = np.array(eval(z2))
            value = np.array(eval(fcn))
        except:
            ee = sys.exc_info()
            error = f'{ee[1]}'
            value = 0
        return(value,z1,z2,error)

    # drawing function (allows drawing of line segments)
    def draw_complex(self, ax, complex_in, color='r'):
        z = complex_in.reshape(-1,1);
        if z.size < 2:
            z = np.vstack((0,z));
            # z = np.hstack((0,z)).reshape(-1,1)

        # split into real and imaginary parts
        Re = [x.real for x in z]
        Im = [x.imag for x in z]
        
        markers_on = np.arange(1,z.size,1)
        ax.plot(Re,Im, '-o', markevery=markers_on, color=color)
    
    
    def update_chart(self):
        result, z1, z2, error = self.get_result()
 
        with plt.style.context(self.plotStyle):
            self.ax.cla()
        if error is None:
            self.z1_real.setText(f"{np.real(z1):.4g}")
            self.z1_imag.setText(f"{np.imag(z1):.4g}")
            self.z1_radius.setText(f"{np.linalg.norm(z1):.4g}")
            self.z1_angle.setText(f"{np.angle(z1)/np.pi:.4g}π")

            self.z2_real.setText(f"{np.real(z2):.4g}")
            self.z2_imag.setText(f"{np.imag(z2):.4g}")
            self.z2_radius.setText(f"{np.linalg.norm(z2):.4g}")
            self.z2_angle.setText(f"{np.angle(z2)/np.pi:.4g}π")

            self.z_real.setText(f"{np.real(result):.4g}")
            self.z_imag.setText(f"{np.imag(result):.4g}")
            self.z_radius.setText(f"{np.linalg.norm(result):.4g}")
            self.z_angle.setText(f"{np.angle(result)/np.pi:.4g}π")
            xm = 10;
            ym = xm // 2;
            with plt.style.context(self.plotStyle):
                self.draw_complex(self.ax, z1, 'b')
                self.draw_complex(self.ax, z2, 'g')
                self.draw_complex(self.ax, result, 'r')
                self.ax.relim()      # make sure all the data fits
                self.ax.autoscale()  # auto-scale
                self.ax.set_xlim([-xm,xm])
                self.ax.set_ylim([-ym,ym])
                self.ax.set_xticks(np.arange(-xm, xm+1, 1.0))
                self.ax.set_yticks(np.arange(-ym, ym+1, 1.0))
                self.ax.set_aspect('equal')
                
                # Move left y-axis and bottim x-axis to centre, passing through (0,0)
                self.ax.spines['left'].set_position('center')
                self.ax.spines['bottom'].set_position('center')
                
                # Eliminate upper and right axes
                self.ax.spines['right'].set_color('none')
                self.ax.spines['top'].set_color('none')
                
                # Show ticks in the left and lower axes only
                self.ax.xaxis.set_ticks_position('bottom')
                self.ax.yaxis.set_ticks_position('left')

                self.ax.legend(('z1','z2','result'))
                self.ax.set_xlabel('imag', labelpad=10)
                self.ax.xaxis.set_label_position('top')
                self.ax.set_ylabel('real',rotation=0, labelpad=30)
                self.ax.yaxis.set_label_position('right')
            self.errorMsg.setText('');
            # self.errorMsg.setAutoFillBackground(False)
        else:
            self.errorMsg.setText( "<font color=red>"+error );
            self.z1_real.setText("")
            self.z1_imag.setText("")
            self.z1_radius.setText("")
            self.z1_angle.setText("")

            self.z2_real.setText("")
            self.z2_imag.setText("")
            self.z2_radius.setText("")
            self.z2_angle.setText("")

            self.z_real.setText("")
            self.z_imag.setText("")
            self.z_radius.setText("")
            self.z_angle.setText("")
            
        self.canvas.draw()
  

    def closeEvent(self,event):
        QApplication.quit()


if __name__ == '__main__':
    # don't auto scale when drag app to a different monitor.
    # QApplication.setAttribute(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance() 
    app.setStyleSheet('''
        QWidget {
            font-size: 30px;
        }
    ''')
    
    myApp = MyApp()
    myApp.show()
    myApp.raise_()

    try:
        if os.environ.get('SPY_UMR_ENABLED'):
            app.exec_()
        else:
            sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')

