# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 14:11:57 2021
 
InteractiveSamplingPlot.py
  Öffnet ein Fenster zur graphischen Visualisierung des Einflusses von 
  Signalabtastung (Signalsampling).
  
  Das Tool erzeugt das Originalsignal und das abgetastete Signal,
  zeigt diese an, und gibt die Signale als Sound aus.

DockPanel: https://www.youtube.com/watch?v=Y6_vNgSyQtU
@author: birkudo
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QDockWidget, QWidget, QLineEdit, QComboBox, QCheckBox, QHBoxLayout, QVBoxLayout, QGridLayout, QPushButton, QLabel, QInputDialog
from PyQt5.QtGui import QColor, QFont
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, pyqtSlot
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib
from scipy import signal
import scipy.fft as fft
import numpy as np
from numpy import *
import sounddevice as sd
import seaborn as sns
import os

class MyFancyButton(QWidget):
    def __init__(self, name, value, keyShortcuts, callback_fcn=None):
        super().__init__()
        self.value = value;
        self.callback_fcn = callback_fcn;
        self.init_layout(name, keyShortcuts);

    def init_layout(self, name, keyShortcuts):
        
        # Create a QHBoxLayout instance
        self.layout = QGridLayout()

        if type(name) == list:
            label = name[0]
            shortcut = name[1]
        else:
            label = name
            shortcut = name[0]
            
        self.label = label
        self.varName = QLabel(label)
        self.layout.addWidget(self.varName, 0, 0)

        if label=='frequency':
            self.valText = QLabel(f"{self.value:.1f} Hz")
        else:
            self.valText = QLabel(f"{self.value:.0f}°")
        self.layout.addWidget(self.valText, 0, 1)
        
        # Add frwd, rwd, edit, fwd, ffwd buttons
        if keyShortcuts[0]:
            self.ButtonFRWD = QPushButton()
            self.ButtonFRWD.setText("<< ["+keyShortcuts[0]+"]")          #text
            self.ButtonFRWD.setShortcut(keyShortcuts[0])  #shortcut key
            self.ButtonFRWD.clicked.connect(lambda:self.on_button([shortcut,'frwd']))
        else:
            self.ButtonFRWD = QLabel('')
        self.layout.addWidget(self.ButtonFRWD, 0, 2)
        
        self.ButtonRWD = QPushButton()
        self.ButtonRWD.setText("< ["+keyShortcuts[1]+"]")          #text
        self.ButtonRWD.setShortcut(keyShortcuts[1])  #shortcut key
        self.ButtonRWD.clicked.connect(lambda:self.on_button([shortcut,'rwd']))
        self.layout.addWidget(self.ButtonRWD, 0, 3)
        
        self.variableButton = QPushButton()
        self.variableButton.setText(f"{self.value:.1f}")           #text
        # self.variableButton.setIcon(QIcon("close.png")) #icon
        self.variableButton.setToolTip(f'edit {label}')
        self.variableButton.setShortcut('Ctrl+'+shortcut)    #shortcut key
        self.variableButton.clicked.connect(lambda:self.on_button([shortcut,'edit']))
        self.layout.addWidget(self.variableButton, 0, 4)

        self.ButtonFWD = QPushButton()
        self.ButtonFWD.setText("["+keyShortcuts[2]+"] >")          #text
        self.ButtonFWD.setShortcut(keyShortcuts[2])  #shortcut key
        self.ButtonFWD.clicked.connect(lambda:self.on_button([shortcut,'fwd']))
        self.layout.addWidget(self.ButtonFWD, 0, 5)
        
        if keyShortcuts[3]:
            self.ButtonFFWD = QPushButton()
            self.ButtonFFWD.setText("["+keyShortcuts[3]+"] >>")          #text
            self.ButtonFFWD.setShortcut(keyShortcuts[3])  #shortcut key
            self.ButtonFFWD.clicked.connect(lambda:self.on_button([shortcut,'ffwd']))
        else:
            self.ButtonFFWD = QLabel('')
        self.layout.addWidget(self.ButtonFFWD, 0, 6)

        self.layout.setColumnStretch(0, 3)
        self.layout.setColumnStretch(1, 3)
        self.layout.setColumnStretch(2, 1)
        self.layout.setColumnStretch(3, 1)
        self.layout.setColumnStretch(4, 3)
        self.layout.setColumnStretch(5, 1)
        self.layout.setColumnStretch(6, 1)
        
        self.setLayout(self.layout)
        
    def set_value(self, value):
        self.value = value;
        self.variableButton.setText(f"{self.value:.1f}")           #text
        if self.label=='frequency':
            self.valText.setText(f"{self.value:.1f} Hz")
        else:
            self.valText.setText(f"{self.value:.0f}°")
        
        
    def set_callback(self, callback_fcn):
        self.callback_fcn = callback_fcn;        
        
    def on_button(self, params):
        # key = params[0];
        # cmd = params[1];
        # print(f'CMD={cmd}, key={key}')

        if self.callback_fcn is not None:
            self.callback_fcn(params)
        


class MyApp(QMainWindow):
    
    # setup discrete signal parameters    
    Fs = 2000.0;      # sampling frequency
    Fs_in = 44100;    # input sampling frequency
    duration = 1.0;   # playback length (sec)
    dur_disp = 0.02;  # time range for display (20 ms)

    # Initialize frequency parameters
    f_init  = 500.0;      # initial frequency
    f_min = 50;
    f_max = 3000;
    f_factor_coarse = 0.02;
    f_factor_fine = 0.0005;

    # Initialize phase parameters
    phase_init = 0.0;      # initial phase
    phase_min = 0;
    phase_max = 360;
    phase_step = 1;


    def __init__(self):
        super().__init__()
        self.f = self.f_init;
        self.phase = self.phase_init;


        self.setWindowTitle("Interactive Sampling Demo UB | Press 'ESC' to quit.")
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)


        # display help:
        self.help()
        
        # self.layout = QVBoxLayout()
        # self.setLayout(self.layout)

        self.plotStyle = 'bmh'
        self.plotStyle = 'fivethirtyeight'

        # initialize data
        self.t_in      = np.linspace(0.0, self.duration, int(self.Fs_in*self.duration+1), endpoint=True);
        self.t_in_disp = self.t_in[self.t_in<=self.dur_disp];
        
        self.t_out      = np.linspace(0.0, self.duration, int(self.Fs*self.duration+1), endpoint=True);
        self.t_out_disp = self.t_out[self.t_out<=self.dur_disp];
        
        # initialize GUI
        GUIlayout = self.init_GUI_panel()
        self.dock = QWidget()
        self.dock.setLayout(GUIlayout)
        # setting feature to the dock

        self.canvas = FigureCanvas(plt.Figure(figsize=(15, 6)))
        self.setCentralWidget(self.canvas)
        
        self.dockWidget=QDockWidget('',self)        
        self.dockWidget.setFloating(False)
        self.dockWidget.setWidget(self.dock)
        # features DockWidgetVerticalTitleBar,DockWidgetClosable,DockWidgetMovable,DockWidgetFloatable
        self.dockWidget.setFeatures(QDockWidget.DockWidgetVerticalTitleBar)
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.dockWidget)

        self.insert_ax()
        
        self.update_chart()
        

    def init_GUI_panel(self):
        # Create a QFormLayout instance
        layout = QGridLayout()

        # Add widgets to the layout
        self.frequencyButton = MyFancyButton("frequency",self.f,['A','S','W','Q'])
        layout.addWidget(self.frequencyButton, 0, 0)
        self.frequencyButton.set_callback(self.on_button)

        self.phaseButton = MyFancyButton(["phase","p"],self.phase,['','D','E',''])
        layout.addWidget(self.phaseButton, 1, 0)
        self.phaseButton.set_callback(self.on_button)
        
# %   [Q]/[A] increases/decreases the original signal's frequency (coarse).
# %   [W]/[S] increases/decreases the original signal's frequency (fine).
# %   [E]/[D] increases/decreases the original signal's phase.
# %   [I]     plays a one-second sound of the original signal.
# %   [O]     plays a one-second sound of the reconstructed signal.
# %   [R]     resets all parameters to their initial values.
# %   [ESC]   exits the script.

        layout.addWidget(QLabel("Fs ="), 0, 1)
        layout.addWidget(QLabel("{} Hz".format(self.Fs)), 0, 2)
        
        layout.addWidget(QLabel("Fs/2 ="), 1, 1)
        layout.addWidget(QLabel("{} Hz".format(self.Fs/2)), 1, 2)
        
        # Another way to make the button invisible is:
        # https://stackoverflow.com/questions/17645319/make-qpushbutton-invisible-yet-still-work
        # ui->errorMask->setStyleSheet("QPushButton { background-color: rgba(10, 0, 0, 0); }");
        self.resetButton = QPushButton("Reset [R]")
        self.resetButton.setToolTip('reset to initial values') 
        self.resetButton.clicked.connect(lambda:self.on_button(['r','reset']))
        self.resetButton.setShortcut('R')  #shortcut key
        layout.addWidget(self.resetButton,0, 3)
        
        self.quitButton = QPushButton("Quit [ESC]")
        self.quitButton.setToolTip('Quit app') 
        self.quitButton.clicked.connect(lambda:self.on_button(['Escape','quit']))
        self.quitButton.setShortcut('Escape')  #shortcut key
        layout.addWidget(self.quitButton,1, 3)
        
        self.playButton1 = QPushButton("Play input [I]")
        self.playButton1.setToolTip('play input signal as audio') 
        self.playButton1.clicked.connect(self.play_audioInput)
        self.playButton1.setShortcut('I')  #shortcut key
        layout.addWidget(self.playButton1,0, 3)

        self.playButton2 = QPushButton("Play output [O]")
        self.playButton2.setToolTip('play output signal as audio') 
        self.playButton2.clicked.connect(self.play_audioOutput)
        self.playButton2.setShortcut('O')  #shortcut key
        layout.addWidget(self.playButton2,1, 3)

        layout.setColumnStretch(0, 6)
        layout.setColumnStretch(1, 1)
        layout.setColumnStretch(2, 1)
        layout.setColumnStretch(3, 3)
        # self.gridLayout.setRowStretch(0, 3)
        # self.gridLayout.setRowStretch(1, 1)
    
        return(layout)  


    def on_button(self, params):
        key = params[0];
        cmd = params[1];
        # print(f'CMD={cmd}, key={key}')
        btn = None
        # if c == 'q', sd.f = min(sd.f*(1+sd.f_factor_coarse),sd.f_max); end
        # if c == 'a', sd.f = max(sd.f*(1-sd.f_factor_coarse),sd.f_min); end
        # if c == 'w', sd.f = min(sd.f*(1+sd.f_factor_fine),sd.f_max); end
        # if c == 's', sd.f = max(sd.f*(1-sd.f_factor_fine),sd.f_min); end
        # if c == 'e', sd.phi = min(sd.phi+sd.phi_step,sd.phi_max); end
        # if c == 'd', sd.phi = max(sd.phi-sd.phi_step,sd.phi_min); end
        # if c == 'r', sd.f = sd.f_init; sd.phi = sd.phi_init; end
        # if c == 'i', try sound(sd.s_long_in,sd.fs_in), catch, end, end
        # if c == 'o', try sound(sd.s_long_out,sd.fs_out), catch, end, end
        # if c == ' ', try clear sound, catch, end, end
        # if strcmp(c,'escape'), windowClose(hFig); end
        if key == 'f':
            btn = self.frequencyButton
            if cmd == 'ffwd':
                self.f = np.minimum(self.f*(1+self.f_factor_coarse),self.f_max);
            if cmd == 'frwd':
                self.f = np.maximum(self.f*(1-self.f_factor_coarse),self.f_min);
            if cmd == 'fwd':
                self.f = np.minimum(self.f*(1+self.f_factor_fine),self.f_max);
            if cmd == 'rwd':
                self.f = np.maximum(self.f*(1-self.f_factor_fine),self.f_min);
            if cmd == 'edit':
                self.showInputDialog(key)
            btn.set_value(self.f)
            self.update_chart()
        if key == 'p':
            btn = self.phaseButton
            if cmd == 'fwd':
                self.phase = np.minimum(self.phase+self.phase_step,self.phase_max);
            if cmd == 'rwd':
                self.phase = np.maximum(self.phase-self.phase_step,self.phase_min);
            if cmd == 'edit':
                self.showInputDialog(key)
            btn.set_value(self.phase)
            self.update_chart()
        if key == 'r':
            self.f = self.f_init; self.phase = self.phase_init;
            self.frequencyButton.set_value(self.f)
            self.phaseButton.set_value(self.phase)
            self.update_chart()
        if key == 'Escape':
            self.close()
        if btn is None:
            return
        
    def showInputDialog(self, key):
        dlg_title = 'Input dialog';
        if key == 'f':
            prompt = 'Enter new frequency:'
            val = f'{self.f:.1f}'
        else:
            prompt = 'Enter new phase:'
            val = f'{self.phase:.0f}'
        text, ok = QInputDialog.getText(self, dlg_title, prompt, text=val)
        if ok:
            try:
                newValue = float(text);
            except:
                ee = sys.exc_info()
                error = f'{ee[1]}'
                print("Error: ", error)
                newValue = None
            if newValue is not None:
                if key == 'f':
                    self.f = np.minimum(np.maximum(newValue,self.f_min),self.f_max);
                    self.frequencyButton.set_value(newValue)
                else:
                    self.phase = np.minimum(np.maximum(newValue,self.phase_min),self.phase_max);
                    self.phaseButton.set_value(newValue)
        self.setFocus(True)
        self.activateWindow()
        self.raise_()
        self.show()

            
    def help(self):
        print("Instructions:");
        print("=============");
        print("Use the following keyboard shortcuts");
        print("[Q]/[A] increases/decreases the original signal's frequency (coarse).");
        print("[W]/[S] increases/decreases the original signal's frequency (fine).");
        print("[E]/[D] increases/decreases the original signal's phase.");
        print("[I]     plays a one-second sound of the original signal.");
        print("[O]     plays a one-second sound of the reconstructed signal.");
        print("[R]     resets all parameters to their initial values.");
        print("[ESC]   exits the script.");
        
        
        
    def insert_ax(self):
        # new font settings
        font = {
            'weight': 'normal',
            'size': 16
        }
        # matplotlib.rc('font', **font)

        with plt.style.context(self.plotStyle):
            ax = self.canvas.figure.add_subplot(111)
            # self.ax.set_position([0.125, 0.13, 0.775, 0.77])
            ax.set_title('Signal(t)');
            ax.set_xlabel('Time(ms)')
            ax.set_ylabel('Signal value')
            for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                    ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(font['size'])        
                item.set_fontweight(font['weight'])
            ax.set_facecolor((0.0, 0.0, 0.0))
            self.ax = ax;

        # plt.style.use('fivethirtyeight')        
        # self.hIn = self.ax.plot(self.t,self.data,'y')[0];
        self.hLine = self.ax.plot([0,20],[0,0],'w',lw=2)[0]
        self.hIn = self.ax.plot(self.t_in_disp*1000.,np.nan*self.t_in_disp,'r',lw=3)[0];
        self.hOut = self.ax.plot(self.t_out_disp*1000.,np.nan*self.t_out_disp,
                'o', ls='-',lw=3, color=[0.3,0.9,1,1],
                markersize=12, markerfacecolor='none', markeredgewidth=2, markeredgecolor='w')[0];
        ax.legend([self.hIn, self.hOut], ['input signal', 'output (sampled)'], 
                  loc='center right', bbox_to_anchor=(1.125, 0.8))
        self.ax.set_xlim(0,self.dur_disp*1000.);
        self.ax.set_ylim(-1.1,1.1);
        
        # self.canvas.figure.tight_layout()

    def update_chart(self):
        
        # Compute input/output signals
        self.signal_in = np.sin(2*np.pi*self.t_in*self.f+self.phase*np.pi/180.0);
        self.s_in_disp = self.signal_in[self.t_in <= self.dur_disp];
        self.signal_out = np.sin(2*np.pi*self.f*self.t_out+self.phase*np.pi/180.0);
        self.s_out_disp = self.signal_out[self.t_out <= self.dur_disp];
        #  Plot input/output signals & sample markers        
        self.hIn.set_ydata(self.s_in_disp)
        self.hOut.set_ydata(self.s_out_disp)
        
        # self.canvas.figure.canvas.draw_idle()
        # self.canvas.figure.canvas.flush_events()
        self.canvas.figure.canvas.draw();
        self.canvas.figure.canvas.flush_events()
        
    def play_audioInput(self):
        sd.play(self.signal_in, self.Fs_in)
    def play_audioOutput(self):
        sd.play(self.signal_out, self.Fs)
        

    def closeEvent(self, event):
        event.accept()
        print('Window closed. Done.')
        QApplication.quit()
        

if __name__ == '__main__':
    # don't auto scale when drag app to a different monitor.
    # QApplication.setAttribute(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance() 
    app.setStyleSheet('''
        QWidget {
            font-size: 30px;
        }
    ''')
    
    myApp = MyApp()
    myApp.show()
    myApp.raise_()

    try:
        if os.environ.get('SPY_UMR_ENABLED'):
            app.exec_()
        else:
            sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')

