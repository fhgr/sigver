# SIGVER

Tools and snippets for the lecture series SIGNALVERARBEITUNG

contains:

_________________

**[interactiveComplexNumberPlot.py](./interactiveComplexNumberPlot.py)**  
A GUI application to display a complex number (e.g. 1+2j) in the complex number plane. Numbers are displayed numerically (Cartesian: Real and Imaginary part, Polar: Radius and Angle), and in vectorial representation. The user can define two different numbers, and modify these numbers, e.g. by adding or multiplying them. Illustrates relationship between cartesian and polar coordinate representation.
![interactiveComplexNumberPlot screenshot](./interactiveComplexNumberPlot_Screenshot.png "interactiveComplexNumberPlot_Screenshot")

_________________

**[interactiveSinePlot.py](./interactiveSinePlot.py)**  
A GUI application to plot a parametric function (e.g. sine or cosine wave), allowing the user to set the frequency, amplitude, sampling frequency and the number of samples. Illustrates relationship between sample number and time
![interactiveSinePlot screenshot](./interactiveSinePlot_Screenshot.png "interactiveSinePlot_Screenshot")

_________________

**[pySpectraUB.py](./pySpectraUB.py)**  
A GUI application to display a real-valued or complex-valued signal, e.g. exp(2*pi*i/64*n). Views of the signal can be real, imaginary, 3d (both real and imaginary) and project (along the time-axis).
![pySpectraUB screenshot](./pySpectraUB_Screenshot.png "pySpectraUB_Screenshot")

_________________

**[interactiveFilterPlot.py](./interactiveFilterPlot.py)**  
A GUI application to visualize the influence of an FIR filter on a noisy signal (e.g. sine or square wave), allowing the user to insert / design the filter numerically. The original and filtered signals are plotted together with the impulse response function (i.e. the filter) and the fourier-transform or the filter.
![interactiveFilterPlot screenshot](./interactiveFilterPlot_Screenshot.png "interactiveFilterPlot_Screenshot")

_________________

**[interactiveSamplingDemo.py](./interactiveSamplingDemo.py)**  
A GUI application to visualize the influence of sampling of a harmonic signal (i.e. sine wave). While the sampling frequency is fixed at 2000 Hz, the user may increase or decrease the frequency (and phase) of the sine wave. The original (input) signal and the sampled (output) signal are plotted. Both signals can be played back as audio signals.
![interactiveSamplingDemo screenshot](./interactiveSamplingDemo_Screenshot.png "interactiveSamplingDemo_Screenshot")
