# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 14:11:57 2021
 
InteractiveSamplingPlot.py
  Öffnet ein Fenster zur graphischen Visualisierung des Einflusses von 
  Signalabtastung (Signalsampling).
  
  Das Tool erzeugt das Originalsignal und das ab,
  das gefilterte Signal (Faltung), sowie den Frequenzgang des Filters an.

 myFIRfilter
 Demo implementation of a finite impuls response filter
 applied to a computer generated signal
                    
https://learndataanalysis.org/how-to-update-a-matplotlib-graph-in-a-pyqt5-application-pyqt5-tutorial/
https://zetcode.com/pyqt/qlineedit/
DockPanel: https://www.youtube.com/watch?v=Y6_vNgSyQtU
@author: birkudo
"""

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QDockWidget, QWidget, QLineEdit, QComboBox, QCheckBox, QHBoxLayout, QVBoxLayout, QGridLayout, QPushButton, QLabel
from PyQt5.QtGui import QColor, QFont
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, pyqtSlot
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib
from scipy import signal
import scipy.fft as fft
import numpy as np
from numpy import *
import seaborn as sns
import os


class MyApp(QMainWindow):
    
    # setup discrete signal parameters
    maxLen = 500;     # number of samples
    noiseLevel = 0.2; 
    addNoise = True;
    yd = 0.01;        # direction and slope  >0: up,  <0: down
    signalType = 'random';
    signalPeriod = 2*np.pi/maxLen*3;
    kernel = np.array([0.25, 0.5, 0.75, 1.0, 0.75, 0.5, 0.25]);
    error = None;

    def __init__(self):
        super().__init__()
        self.setWindowTitle('Interactive Filter Plot UB')
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)

        # self.layout = QVBoxLayout()
        # self.setLayout(self.layout)

        self.plotStyle = 'bmh'

        color  = QColor(255, 32, 32)
        alpha  = 140
        self.errorColor = "{r}, {g}, {b}, {a}".format(r = color.red(),
                                             g = color.green(),
                                             b = color.blue(),
                                             a = alpha
                                             )

        # initialize data
        self.t = np.linspace(0.,10.,self.maxLen);
        self.ydata = np.nan*self.t;   # start with empty data array
        self.y = np.random.rand(1);   # random y start value
        self.yi = 0;                  # index into ydata
        
        # initialize GUI
        GUIlayout = self.init_GUI_panel()
        self.dock = QWidget()
        self.dock.setLayout(GUIlayout)
        # setting feature to the dock

        self.canvas = FigureCanvas(plt.Figure(figsize=(15, 6)))
        self.setCentralWidget(self.canvas)
        
        # edit parameters in a dockwindow
        self.dockWidget=QDockWidget('',self)        
        self.dockWidget.setFloating(False)
        self.dockWidget.setWidget(self.dock)
        # features DockWidgetVerticalTitleBar,DockWidgetClosable,DockWidgetMovable,DockWidgetFloatable
        self.dockWidget.setFeatures(QDockWidget.DockWidgetVerticalTitleBar)
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.dockWidget)

        self.insert_ax()
        
        self.onFilterKernel()

        self._timer = QTimer()
        self._timer.timeout.connect(self.onTimeout)
        self._timer.start(25)


    def init_GUI_panel(self):
        # Create a QFormLayout instance
        layout = QGridLayout()

        # Add widgets to the layout
        layout.addWidget(QLabel("Signal"), 0, 0)
        layout.addWidget(QLabel("Filter"), 0, 2)
        
        self.comboSig = QComboBox(self)
        self.comboSig.addItem('random')
        self.comboSig.addItem('sine')
        self.comboSig.addItem('square')
        self.comboSig.setSizeAdjustPolicy(1)
        self.comboSig.setStyleSheet('QComboBox {font: 22pt Arial}')
        self.comboSig.setEditable(False)
        layout.addWidget(self.comboSig, 0, 1)
        self.comboSig.activated[str].connect(self.onSignalType)
        
        self.input_kernel = QLineEdit(f'{list(self.kernel)}')
        self.input_kernel.setToolTip('enter filter kernel') 
        layout.addWidget(self.input_kernel, 0, 3)

        self.add_noise = QCheckBox("noise")
        self.add_noise.setChecked(True)
        self.add_noise.stateChanged.connect(lambda:self.onButton(self.add_noise))
        layout.addWidget(self.add_noise, 1, 0)
        self.input_noiseLevel = QLineEdit('0.2')
        self.input_noiseLevel.setToolTip('enter noise level (1.0=100%)') 
        layout.addWidget(self.input_noiseLevel, 1, 1)

        self.errorMsg = QLabel('')
        layout.addWidget(self.errorMsg, 1, 3)

        layout.setColumnStretch(0, 1)
        layout.setColumnStretch(1, 1)
        layout.setColumnStretch(2, 1)
        layout.setColumnStretch(3, 8)
        # self.gridLayout.setRowStretch(0, 3)
        # self.gridLayout.setRowStretch(1, 1)
    
        self.input_kernel.returnPressed.connect(self.onFilterKernel)
        self.input_noiseLevel.returnPressed.connect(self.onNoiseLevel)

        return(layout)        

    def insert_ax(self):
        # new font settings
        font = {
            'weight': 'normal',
            'size': 16
        }
        # matplotlib.rc('font', **font)

        with plt.style.context(self.plotStyle):
            ax = self.canvas.figure.add_subplot(2,3,(1,2))
            # self.ax.set_position([0.125, 0.13, 0.775, 0.77])
            ax.set_title('Signal(t)');
            for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                    ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(font['size'])        
                item.set_fontweight(font['weight'])
            ax.set_facecolor((0.0, 0.0, 0.0))
            self.axOrig = ax;
            ax = self.canvas.figure.add_subplot(2,3,(4,5))
            ax.set_title('gefiltered (Faltung mit Filter)');
            ax.set_xlabel('Zeit');
            for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                    ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(font['size'])        
                item.set_fontweight(font['weight'])
            ax.set_facecolor((0.0, 0.0, 0.0))
            self.axFiltered = ax;
        ax = self.canvas.figure.add_subplot(2,3,3)
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(font['size'])        
            item.set_fontweight(font['weight'])
        self.axKernel = ax;
        ax = self.canvas.figure.add_subplot(2,3,6)
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(font['size'])        
            item.set_fontweight(font['weight'])
        self.axSpectra = ax;
        # plt.style.use('fivethirtyeight')
        
        self.hp = self.axOrig.plot(self.t,self.ydata,'y')[0];
        self.axOrig.set_xlim(0,10);
        self.axOrig.set_ylim(0-self.noiseLevel,1+self.noiseLevel);
        
        self.hf = self.axFiltered.plot(self.t,self.ydata,'y')[0];
        self.axFiltered.set_xlim(0,10);
        self.axFiltered.set_ylim(0-self.noiseLevel,1+self.noiseLevel);

        self.canvas.figure.tight_layout()

    def onSignalType(self, text):
        error = 'unknown input signal';
        if text == 'random':
            error = None;
        if text == 'sine':
            error = None;
        if text == 'square':
            error = None;
        if error is not None:
            self.error = None;
            return;
        self.signalType = text;

    def onButton(self, b):
       if b.text() == "noise":
           if b.isChecked() == True:
              print(b.text()+" is selected")
              self.addNoise = True;
           else:
              print(b.text()+" is deselected")
              self.addNoise = False;

    def onFilterKernel(self):
        text = self.input_kernel.text();
        try:
            self.kernel = np.array(eval(text));
            self.kernel = np.hstack(([0.,0.,0.],self.kernel,[0.,0.,0.]))/np.sum(np.abs(self.kernel))
            self.error = None;
        except:
            ee = sys.exc_info()
            self.error = f'{ee[1]}'
            return;
        with plt.style.context(self.plotStyle):
            self.axKernel.cla()
            self.axKernel.stem(np.arange(-3,self.kernel.size-3),self.kernel)
            self.axKernel.set_title('Impulsantwort')
        
        ftKernel = np.zeros(64);
        ftKernel[0:self.kernel.size] = self.kernel;
        FT = fft.fft(ftKernel);
        FT = fft.fftshift(FT);
        with plt.style.context(self.plotStyle):
            self.axSpectra.cla()
            self.axSpectra.plot(np.linspace(-np.pi,np.pi,ftKernel.size, endpoint=False), np.abs(FT))
            self.axSpectra.set_title('|Frequenzgang|')
        
    def onNoiseLevel(self):
        text = self.input_noiseLevel.text();
        self.noiseLevel = eval(text);
        self.axOrig.set_ylim(0-self.noiseLevel-0.1,1+self.noiseLevel+0.1);
        self.axFiltered.set_ylim(0-self.noiseLevel-0.1,1+self.noiseLevel+0.1);
        

    def update_chart(self):
        
        i = 0+1j
        j = i

        error = self.error
        # value = eval(fcn)
        y = self.y; n = 0.;
        if error is None:
            self.yi = self.yi+1;
            if self.addNoise:
                n = self.noiseLevel*(np.random.rand(1)-0.5);
            if self.signalType == 'random':
                if(np.random.rand(1)<0.05):
                    self.yd = -self.yd;
                if (y+self.yd)>1: 
                    self.yd = -np.abs(self.yd);
                if (y+self.yd)<0:
                    self.yd = np.abs(self.yd);
                y = y+self.yd;
            if self.signalType == 'sine':
                y = 0.5+0.5*np.sin(self.signalPeriod*self.yi);
            if self.signalType == 'square':
                y = float(np.sin(self.signalPeriod*self.yi)>0);
            self.errorMsg.setText('');
        else:
            self.errorMsg.setText( "<font color=red>"+error );
        self.y = y;
        if(self.yi >= self.maxLen):
            self.ydata[0:-1] = self.ydata[1:];
            self.ydata[self.maxLen-1]=y+n;
        else:
            self.ydata[self.yi]=y+n;
        startIndex = (self.kernel.size+1)//2
        yfiltered = np.convolve(self.ydata,self.kernel)
        yfiltered = yfiltered[startIndex:(self.maxLen+startIndex)];
        if (self.yi % 20) == 0:
            mask = np.logical_not(np.isnan(yfiltered));
            mmin = np.min(yfiltered[mask])-self.noiseLevel-0.1;
            mmax = np.max(yfiltered[mask])+self.noiseLevel+0.1;
            if mmax-mmin > 0:
                self.axFiltered.set_ylim(mmin,mmax);
        # print(self.ydata[self.yi])
        
        self.hp.set_ydata(self.ydata)
        self.hf.set_ydata(yfiltered)
        
        # self.canvas.figure.canvas.draw_idle()
        # self.canvas.figure.canvas.flush_events()
        self.canvas.figure.canvas.draw();
        self.canvas.figure.canvas.flush_events()
        
        try:
            pass
        except:
            ee = sys.exc_info()
            error = f'{ee[1]}'
            value = 0

    @pyqtSlot()
    def onTimeout(self):
        # print('timeout')
        self.update_chart()
  

    def closeEvent(self, event):
        try:
            self._timer.stop()
            print('timer stopped')
        except:
            pass
        event.accept()
        print('Window closed. Done.')
        QApplication.quit()


if __name__ == '__main__':
    # don't auto scale when drag app to a different monitor.
    # QApplication.setAttribute(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance() 
    app.setStyleSheet('''
        QWidget {
            font-size: 30px;
        }
    ''')
    
    myApp = MyApp()
    myApp.show()
    myApp.raise_()

    try:
        if os.environ.get('SPY_UMR_ENABLED'):
            app.exec_()
        else:
            sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')

