# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 14:11:57 2021

https://learndataanalysis.org/how-to-update-a-matplotlib-graph-in-a-pyqt5-application-pyqt5-tutorial/
@author: birkudo
"""

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit, QHBoxLayout, QVBoxLayout, QFormLayout, QPushButton, QLabel
from PyQt5.QtGui import QColor, QFontDatabase
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib
from scipy import signal
import numpy as np
from numpy import *
import os
import ast
import time

try:
    useSoundDevice = True
    useSoundDevice = False
    sd = None
    if useSoundDevice:
        import sounddevice as sd
except:
    pass

try:
    import pyaudio
except ImportError as error:
    print("pyaudio not found.")
    print("pyaudio is a C library.")
    print("In Anaconda, it cannot be installed with pip. Try:")
    print("   conda install pyaudio")


class myPyAudioPlayer:
    def __init__(self):
        pass;

    def open(self, Fs):
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(
            format = pyaudio.paFloat32,
            channels = 1,
            rate = int(Fs),
            output = True
        )

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Q:
            print("Killing")
            self.deleteLater()
        # elif event.key() == QtCore.Qt.Key_Enter:
        #     self.proceed()
        event.accept()

    def play(self, values, Fs=44100):
        self.open(Fs);
        """ Play entire data """
        self.stream.write(values.astype(np.float32).tobytes())
        time.sleep(len(values)/Fs)
        self.stream.stop_stream()
        self.close()

    def close(self):
        """ Graceful shutdown """ 
        self.stream.close()
        self.p.terminate()

if sd is None:
    sd = myPyAudioPlayer()

class MyApp(QWidget):
    def __init__(self):
        super().__init__()
        self.fixedFont = QFontDatabase.systemFont(QFontDatabase.FixedFont)
        
        self.drawContinuous = False
        self.setWindowTitle('interactive sine plot UB')
        self.window_width, self.window_height = 1200, 800
        self.setMinimumSize(self.window_width, self.window_height)

        self.layout = QVBoxLayout()
        self.setLayout(self.layout)

        color  = QColor(255, 32, 32)
        alpha  = 140
        self.errorColor = "{r}, {g}, {b}, {a}".format(r = color.red(),
                                             g = color.green(),
                                             b = color.blue(),
                                             a = alpha
                                             )

        GUIlayout = self.init_GUI_panel()
        self.layout.addLayout(GUIlayout)

        
        self.canvas = FigureCanvas(plt.Figure(figsize=(15, 6)))
        self.layout.addWidget(self.canvas)

        self.insert_ax()
        self.update_chart()
        

    def init_GUI_panel(self):
        global sd
        # Create a QFormLayout instance
        layout = QFormLayout()
        # Add widgets to the layout
        self.inputFcn = QLineEdit('A*cos(2*pi*f*n)')
        self.inputFcn.setToolTip('enter definition of the signal (e.g. sin(2*pi*f*t)') 
        layout.addRow("Signal function y:", self.inputFcn)

        self.inputDuration = QLineEdit('200')
        self.inputDuration.setToolTip('enter number of samples (e.g. 200)') 
        layout.addRow("Num. samples:", self.inputDuration)

        self.inputFs = QLineEdit('400')
        self.inputDuration.setToolTip('enter sampling frequency in Hertz (e.g. 3000)') 
        layout.addRow("Sampling freq. Fs (Hz):", self.inputFs)

        self.inputF = QLineEdit('1/8')
        self.inputF.setToolTip('enter relative signal frequency (e.g. 1/8)') 
        layout.addRow("Signal freq. f/Fs:", self.inputF)

        self.inputAmpl = QLineEdit('100')
        self.inputAmpl.setToolTip('enter signal amplitude (in %)') 
        layout.addRow("Rel. amplitude A (%):", self.inputAmpl)

        # emailLabel = QLabel("Email:")
        # layout.addRow(emailLabel, QLineEdit())
        hlayout = QHBoxLayout()
        self.drawButton = QPushButton("Draw")
        self.drawButton.setToolTip('update/redraw plot') 
        self.drawButton.clicked.connect(self.update_chart)
        hlayout.addWidget(self.drawButton,1)

        if sd is None:
            sd = myPyAudioPlayer();

        self.playButton = QPushButton("Play")
        self.playButton.setToolTip('play signal as audio') 
        self.playButton.clicked.connect(self.play_audioFs)
        hlayout.addWidget(self.playButton,1)

        self.playFsButton = QPushButton("Play(44.1kHz)")
        self.playFsButton.setToolTip('play signal as audio at Fs=44.1kHz') 
        self.playFsButton.clicked.connect(self.play_audio44)
        hlayout.addWidget(self.playFsButton,1)

        self.errorMsg = QLabel('')
        hlayout.addWidget(self.errorMsg,4)
        layout.addRow(hlayout)

        font = self.inputDuration.font()
        font.setFamily(self.fixedFont.defaultFamily())
        self.inputDuration.setFont(font)
        self.inputFs.setFont(font)
        self.inputF.setFont(font)
        self.inputAmpl.setFont(font)
        self.inputFcn.setFont(font)
        
        self.inputDuration.returnPressed.connect(self.update_chart)
        self.inputFs.returnPressed.connect(self.update_chart)
        self.inputF.returnPressed.connect(self.update_chart)
        self.inputAmpl.returnPressed.connect(self.update_chart)
        self.inputFcn.returnPressed.connect(self.update_chart)

        return(layout)        
        
    def insert_ax(self):
        # backup current font settings
        self.fontRC = {
            'weight': matplotlib.rcParams['font.weight'],
            'size':   matplotlib.rcParams['font.size']
        }
        # new font settings
        font = {
            'weight': 'normal',
            'size': 16
        }
        matplotlib.rc('font', **font)

        self.ax = self.canvas.figure.subplots()
        self.ax.set_ylim([-1.03, 1.03])
        self.ax.set_xlim([0, 1])
        self.ax.set_position([0.125, 0.13, 0.775, 0.77])
        self.fill_between = None
        self.bar = None
        self.line = None
        self.stem = None

    def getFloat(self, inputField, defaultValue = 1.0):
        value = inputField.text()
        value = float(eval(value))
        return(value)

    def get_signal(self, Fs = None, useSampleNumber=True):
        fcn  = self.inputFcn.text()
        
        u     = lambda x: (np.array(x)>=0).astype(np.float)
        step  = lambda x: (np.array(x)>=0).astype(np.float)
        ε     = lambda x: (np.array(x)>=0).astype(np.float)
        delta = lambda x: (np.array(x)==0).astype(np.float)
        δ     = lambda x: (np.array(x)==0).astype(np.float)
        ramp  = lambda x: (np.array(x))*u(x);
        rampn = lambda x: ramp(x)/np.array(x).size; # normalized ramp 0,..,1
        π     = np.pi

        error = None
        n = np.arange(0,200)
        t = n/400.;
        try:
            numSamples   = self.getFloat(self.inputDuration)
            f    = self.getFloat(self.inputF)
            A    = self.getFloat(self.inputAmpl)/100.0
            if Fs is None:
                Fs   = self.getFloat(self.inputFs)
            else:
                numSamples = numSamples*Fs/self.getFloat(self.inputFs)
                f = self.getFloat(self.inputFs)/Fs*f
                print(f'({self.inputF.text()}) * ({self.inputFs.text()})Hz = {(f*Fs):.1f}Hz')
    
            n = np.arange(0,numSamples)
            t = n  # t = n/Fs
            t = n/Fs
            if not useSampleNumber:
                f = f*Fs; # f*self.getFloat(self.inputFs)
    
            value = eval(fcn)
        except:
            Fs = 400;
            ee = sys.exc_info()
            error = f'{ee[1]}'
            value = t*0
        value = np.clip(value, -1.0, 1.0);
        return(n,value,Fs,error)
    
    def useSampleNumber(self):
        sampleNumber = False;
        code  = self.inputFcn.text()
        try:
            st = ast.parse(code)
            for node in ast.walk(st):
                if type(node) is ast.Name:
                    if (node.id=='n'):
                        sampleNumber = True;
        except:
            pass;
        return sampleNumber

    def play_audio(self, Fs_in = None):
        global sd
        sampleNumber = self.update_chart()
        n, values, Fs, error = self.get_signal(Fs_in, useSampleNumber=sampleNumber)
        if Fs_in is None:
            values = signal.resample_poly(values, 44100.0, Fs)
            Fs = 44100.0
        if error is None:
            sd.play(0.95*values, Fs)

    def play_audioFs(self):
        self.play_audio(None)
    def play_audio44(self):
        self.play_audio(44100.0)
        
    def update_chart(self):
        # check whether user uses sample number or time
        sampleNumber = self.useSampleNumber();
        
        n44, values44, Fs44, error44 = self.get_signal(44100, useSampleNumber=sampleNumber);
        n, values, Fs, error = self.get_signal(useSampleNumber=sampleNumber)
        
        xValues = n/Fs;
        axMax = (n[-1]+1)/Fs;
        if sampleNumber:
            xValues = n
            axMax = n[-1]+1;
            
                    
        if self.line:
            self.line.pop(0).remove()
            self.line = None
        if self.bar:
            self.bar.remove()
            self.bar = None
        if self.fill_between:
            self.fill_between.remove()
            self.fill_between = None
        if self.stem:
            self.stem.remove()
            self.stem = None
        if error is None:
            if sampleNumber:
                x44 = n44/Fs44*Fs
            else:
                x44 = n44/Fs44;
            # self.bar = self.ax.bar(x44, values44, width=1.0, color='g', alpha=0.5)
            if self.drawContinuous:
                self.fill_between = self.ax.fill_between(x44, values44, 0, alpha=0.30, color='g', label="'continuous'")
            # self.bar = self.ax.hist(values44, bins=x44, ls='None', alpha = 0.5, lw=3, color= 'g')
            self.stem = self.ax.stem(xValues, values, label='signal') # , use_line_collection=True)
            self.errorMsg.setText('');
            # self.errorMsg.setAutoFillBackground(False)
            self.ax.set_xlim([0, axMax])
            if sampleNumber:
                self.ax.set_xlabel('sample number n')
            else:
                self.ax.set_xlabel('time t (sec)')
            self.ax.set_ylabel('signal y')
            self.ax.legend(); # ["'continous'","signal"])
        else:
            self.errorMsg.setText( "<font color=red>"+error );
            #self.errorMsg.setAutoFillBackground(True)
            #self.setStyleSheet("QLabel { background-color: rgba("+self.errorColor+"); }")
            self.ax.set_xlabel('')
            self.ax.set_ylabel('')
            
        self.canvas.draw()
    
        return sampleNumber


    def closeEvent(self,event):
        matplotlib.rc('font', **self.fontRC)
        QApplication.quit()


if __name__ == '__main__':
    # don't auto scale when drag app to a different monitor.
    # QApplication.setAttribute(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance() 
    app.setStyleSheet('''
        QWidget {
            font-size: 30px;
        }
    ''')
    
    myApp = MyApp()
    myApp.show()
    myApp.raise_()

    try:
        if os.environ.get('SPY_UMR_ENABLED'):
            app.exec_()
        else:
            sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')

