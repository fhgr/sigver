# -*- coding: utf-8 -*-
"""
Created on Tue Aug 26 09:08:31 2021

Interactive GUI for plotting signals and their fourier transform
    - allows arbitrary custom signals (e.g. sine, exponential, etc.)
    - visualizes real valued and complex signals
    -       real and imaginary part
    - allows 3D view (time-axis, real-, and imaginary part)
    - allows projection of complex signals onto the complex plane 
            (i.e. along the time axis)
    - plotting of pseudo-"continuous" time signals (i.e. high sampling rate)
    - visualizes fourier transform
    -       magnitude and phase
    -       real and imaginary part

Chnages:
2021-10-12: replace numpy fft with scipy fft
            exchange FFT bar plot with 2D line plot for speed optimization
2021-11-01: inserted ICON and scientific y-Ticks in Spectra
            
@author: birkudo
"""
__author__ = "Udo Birk"
__copyright__ = "Copyright 2021, FH Graubünden"
__credits__ = ["Udo Birk"]
__license__ = "MIT"
__version__ = "1.1.2"
__maintainer__ = "Udo Birk"
__email__ = "udo.birk@fhgr.ch"
__status__ = "Production"

import sys
try:
    from packaging import version
except ImportError:
    raise ImportError('Module packaging not found. Please install "packaging".')
try:
    from PyQt5 import QtGui
    from PyQt5 import QtWidgets
    from PyQt5.QtCore import Qt, QTimer
except ImportError:
    raise ImportError('Module PyQt5 not found. Please install "pyqt5".')
import signal
import numpy as np
import base64
from numpy import *
try:
    import matplotlib.pyplot as plt
except ImportError:
    raise ImportError('Module matplotlib not found. Please install "matplotlib".')
if(version.parse(plt.matplotlib.__version__) < version.parse("3.1.1")):
    raise ImportError('Module matplotlib found, but version is < 3.1.1. Please update "matplotlib".')
from mpl_toolkits import mplot3d
from matplotlib.widgets import Slider, Button, RadioButtons, TextBox
from matplotlib.figure import Figure
from matplotlib.ticker import AutoLocator
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
#from PyQt5.QtGui import *
#from PyQt5.QtWidgets import *

import random
try:
    import scipy.fft
except ImportError:
    raise ImportError('Module scipy not found. Please install "scipy".')


styleString = "font: 10pt;"
PRINTDEBUG  = False;

class stem3:
    ax = None;
    stem = None;
    markerSymbol = '';
    markerSize = 4;
    
    def __init__(self, ax, x, y, z, markerSymbol='o', markerSize=6, **kwargs):
        self.ax = ax;
        self.markerSymbol = markerSymbol;
        self.markerSize = markerSize;
        self.plotStem(x, y, z);
        
    def plotStem(self, x, y, z):
        x2=np.stack((x,x,np.nan*x)).flatten('F');
        y2=np.stack((y,0.0*np.array(y),np.nan*y)).flatten('F');
        z2=np.stack((z,0.0*np.array(z),np.nan*x)).flatten('F');
        self.stem, = self.ax.plot3D(x2,y2,z2,marker=self.markerSymbol,ls='-',ms=self.markerSize,markevery=(0,3));
        # hline = art3d.Line3D(x2,y2,z2,marker='o',markevery=(0,3));
        # self.ax.add_line(hline);
        # self.hstem=lines[0];
        self.stem.set_color('#ff7f0e'); # '#ff7f0e')  '#1f77b4'
        self.stem.set_markerfacecolor('#1f77b4')
        self.stem.set_markeredgecolor('none')
        
    def set_ms(self, markerSize):   # set marker size
        self.markerSize = markerSize;
        self.stem.set_markersize(self.markerSize);
    
    def set_marker(self, markerSymbol):
        self.markerSymbol = markerSymbol;
        self.stem.set_marker(markerSymbol);
    
    def show_marker(self, bShowMarker=True):
        if(bShowMarker):
            self.stem.set_marker(self.markerSymbol);
        else:
            self.stem.set_marker('');
    
    def set_ls(self, lineStyle):
        self.stem.set_linestyle(lineStyle);
    
    def show_line(self, bShowLine=True):
        if(bShowLine):
            self.stem.set_linestyle('-');
        else:
            self.stem.set_linestyle('');
    
    def setData(self, x,y,z):
        x2=np.stack((x,x,np.nan*x)).flatten('F');
        y2=np.stack((y,0.0*np.array(y),np.nan*y)).flatten('F');
        z2=np.stack((z,0.0*np.array(z),np.nan*x)).flatten('F');
        self.stem.set_data_3d(x2, y2, z2);
        

class spectraPlot(object):
    def __init__(self, ax, x, y):
        self.rects = None;
        self.ax = ax;
        self.drawNew(x,y);
    
    def drawNew(self, x, y, w=None, wScale=1.0):
        mask = np.logical_not(np.isnan(y))
        x2 = x[0::3];
        # w = np.mean(np.diff(x2))*0.7;
        if w is None:
            w = np.mean(np.diff(x2))*150.0;
            w = np.min( (10.0,np.max( (0.3,w) )) );        
        
        self.line  = self.ax.plot(x2,0*x2,color='b',linewidth=0.5)[0]
        # self.rects = self.ax.bar(x,y,w,color='b',linewidth=0*x+3.0)
        self.rects = self.ax.plot(x,y,color='b',linewidth=w*wScale)[0]
        
        mask = np.logical_not(np.isnan(y))
        y2 = y[mask];
        if y2.size == 0:
            y2 = 0
        h = np.max(np.abs(y2));
        if h > 1:
            pass
        elif h >= 0.499:
            h = 1.0;
        h = 1.05*np.max( (h) );
        if (h == 0):
            h = 0.002;
        self.ax.set_ylim(np.array([-1,1])*h)
   
    def remove(self):
        if self.line is not None:
            self.line.remove();
            self.line = None;
        if self.rects is not None:
            self.rects.remove();
            self.rects = None;
        # self.ax.cla();
        
    def set_data(self, x, y, w=None, wScale=1.0):
        if self.rects is not None:
            self.remove();
        self.drawNew(x,y, w, wScale)
        # elif self.rects.get_children() == len(x):
        #     for rect, h in zip(self.rects, x):
        #         rect.set_height(h)
        # else:
        #    self.drawNew(x,y);


class GraphPanel(QtWidgets.QFrame):
    global styleString
    # member variables
    mainWin = None;
    axMain = None;
    labelStyleSheet = styleString

    def __init__(self, mainWin, parent=None):
        self.mainWin = mainWin;
        super(QtWidgets.QFrame, self).__init__(parent)
        self.initLayout();
        
    def initLayout(self):
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        # a figure instance to plot on
        self.fig = Figure()
        # self.fig.tight_layout()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.fig)
        
        self.panelRight = QtWidgets.QFrame()
        #< self.panelRight.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.panelRight.setFixedWidth(90)

        # set the layout
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0); # (left, top, right, bottom)

        # layout.addWidget(self.toolbar)
        self.layout.addWidget(self.canvas)
        self.layout.addWidget(self.panelRight)
        # layout.addWidget(self.button)
        self.setLayout(self.layout)
        
    def addSignalPanelItems(self):
        # set the layout
        self.rightlayout = QtWidgets.QVBoxLayout()
        self.rightlayout.setContentsMargins(0,0,0,0); # (left, top, right, bottom)
        
        self.groupboxMapping = QtWidgets.QGroupBox(); # 
        self.groupboxMapping.setStyleSheet(self.labelStyleSheet);
        self.radioMapReal = QtWidgets.QRadioButton("real")
        self.radioMapReal.toggled.connect(lambda:self.mainWin.on_datadisplaymode(self.radioMapReal))
        self.radioMapImag = QtWidgets.QRadioButton("imaginary")
        self.radioMapImag.toggled.connect(lambda:self.mainWin.on_datadisplaymode(self.radioMapImag))
        self.radioMap3d = QtWidgets.QRadioButton("3d")
        self.radioMap3d.toggled.connect(lambda:self.mainWin.on_datadisplaymode(self.radioMap3d))
        self.radioMapProject = QtWidgets.QRadioButton("project")
        self.radioMapProject.toggled.connect(lambda:self.mainWin.on_datadisplaymode(self.radioMapProject))
        self.radioMapReal.setChecked(True)
        vbox1 = QtWidgets.QVBoxLayout()
        vbox1.addWidget(self.radioMapReal)
        vbox1.addWidget(self.radioMapImag)
        vbox1.addWidget(self.radioMap3d)
        vbox1.addWidget(self.radioMapProject)
        self.groupboxMapping.setLayout(vbox1)
        
        self.checkBoxShowOptions = QtWidgets.QCheckBox("Options")
        self.checkBoxShowOptions.setStyleSheet(self.labelStyleSheet);
        self.checkBoxShowOptions.stateChanged.connect(lambda:self.mainWin.on_buttonstate(self.checkBoxShowOptions))
        
        self.groupboxProject = QtWidgets.QGroupBox(); # 
        self.groupboxProject.setStyleSheet(self.labelStyleSheet);
        self.radioProjectOrtho = QtWidgets.QRadioButton("orthographic")
        self.radioProjectOrtho.toggled.connect(lambda:self.mainWin.on_buttonstate(self.radioProjectOrtho))
        self.radioProjectPerspect = QtWidgets.QRadioButton("perspective")
        self.radioProjectPerspect.toggled.connect(lambda:self.mainWin.on_buttonstate(self.radioProjectPerspect))
        self.radioProjectOrtho.setChecked(True)
        vbox2 = QtWidgets.QVBoxLayout()
        vbox2.addWidget(self.radioProjectOrtho)
        vbox2.addWidget(self.radioProjectPerspect)
        self.groupboxProject.setLayout(vbox2)

        self.groupboxStyle = QtWidgets.QGroupBox(); # 
        self.groupboxStyle.setStyleSheet(self.labelStyleSheet);
        self.checkBoxStyleMarker = QtWidgets.QCheckBox("Marker")
        self.checkBoxStyleMarker.toggled.connect(lambda:self.mainWin.on_buttonstate(self.checkBoxStyleMarker))
        self.radioStyleOMarker = QtWidgets.QRadioButton("o Marker")
        self.radioStyleOMarker.toggled.connect(lambda:self.mainWin.on_buttonstate(self.radioStyleOMarker))
        self.radioStyleAMarker = QtWidgets.QRadioButton("* Marker")
        self.radioStyleAMarker.toggled.connect(lambda:self.mainWin.on_buttonstate(self.radioStyleAMarker))
        self.radioStyleOMarker.setChecked(True)
        self.checkBoxStyleConnect = QtWidgets.QCheckBox("connect")
        self.checkBoxStyleConnect.toggled.connect(lambda:self.mainWin.on_buttonstate(self.checkBoxStyleConnect))
        self.checkBoxStyleStem = QtWidgets.QCheckBox("stem")
        self.checkBoxStyleStem.toggled.connect(lambda:self.mainWin.on_buttonstate(self.checkBoxStyleStem))
        self.checkBoxStyleMarker.setChecked(True)
        self.checkBoxStyleConnect.setChecked(True)
        self.checkBoxStyleStem.setChecked(True)
        vbox3 = QtWidgets.QVBoxLayout()
        vbox3.addWidget(self.checkBoxStyleMarker)
        vbox3.addWidget(self.radioStyleOMarker)
        vbox3.addWidget(self.radioStyleAMarker)
        vbox3.addWidget(self.checkBoxStyleConnect)
        vbox3.addWidget(self.checkBoxStyleStem)
        self.groupboxStyle.setLayout(vbox3)

        self.checkBoxShowTransform = QtWidgets.QCheckBox("Transform")
        self.checkBoxShowTransform.setStyleSheet(self.labelStyleSheet);
        self.checkBoxShowTransform.hide()
        self.checkBoxShowTransform.stateChanged.connect(lambda:self.mainWin.on_buttonstate(self.checkBoxShowTransform))
        
        self.rightlayout.addWidget(self.groupboxMapping);
        self.rightlayout.addWidget(self.checkBoxShowOptions);
        self.rightlayout.addWidget(self.groupboxProject);
        self.rightlayout.addWidget(self.groupboxStyle);
        self.rightlayout.addWidget(self.checkBoxShowTransform);
        
        self.rightlayout.addStretch(1)
        
        self.panelRight.setLayout(self.rightlayout)
        
        self.axMain = self.fig.add_subplot(111, projection='3d', proj_type='ortho')
        # self.axMain = plt.subplot(111, projection='3d', proj_type = 'ortho') # fig.subplots()
        # self.fig.subplots_adjust(left=0.12, bottom=0.10, top=0.9, right=0.9)
        self.fig.subplots_adjust(left=0.025, right=0.975, bottom=0.05, top=0.95)
        # self.axMain.set_aspect('equal', 'box')
        
        self.axYLabel = self.axMain.set_ylabel('Re{ Signal x[n]}')
        self.axXLabel = self.axMain.set_xlabel('n')
        self.axZLabel = self.axMain.set_zlabel('Im{ Signal x[n]}')

    def addSpectraPanelItems(self):
        self.mapping = "magnitude";
        self.range   = "Full";
        
        # set the layout
        self.rightlayout = QtWidgets.QVBoxLayout()
        self.rightlayout.setContentsMargins(0,0,0,0); # (left, top, right, bottom)

        self.groupboxPrecision = QtWidgets.QGroupBox(); # 
        self.groupboxPrecision.setStyleSheet(self.labelStyleSheet);
        labelPrecision = QtWidgets.QLabel('Precision:')
        self.editPrecision = QtWidgets.QLineEdit()
        self.editPrecision.setText(self.mainWin.precision_text);
        self.editPrecision.setStyleSheet('font: 9pt "Courier";');
        font = self.editPrecision.font()    # lineedit current font
        font.setPointSize(7)                # change it's size
        self.editPrecision.setFont(font)    # set font
        self.editPrecision.setAlignment(Qt.AlignCenter);
        vbox3 = QtWidgets.QVBoxLayout()
        vbox3.addWidget(labelPrecision)
        vbox3.addWidget(self.editPrecision)
        self.groupboxPrecision.setLayout(vbox3)
        
        self.groupboxMapping = QtWidgets.QGroupBox(); # 
        self.groupboxMapping.setStyleSheet(self.labelStyleSheet);
        self.radioMapMagnitude = QtWidgets.QRadioButton("magnitude")
        self.radioMapMagnitude.toggled.connect(lambda:self.mainWin.on_spectradisplaymode(self.radioMapMagnitude))
        self.radioMapPhase = QtWidgets.QRadioButton("phase")
        self.radioMapPhase.toggled.connect(lambda:self.mainWin.on_spectradisplaymode(self.radioMapPhase))
        self.radioMapReal = QtWidgets.QRadioButton("real")
        self.radioMapReal.toggled.connect(lambda:self.mainWin.on_spectradisplaymode(self.radioMapReal))
        self.radioMapImag = QtWidgets.QRadioButton("imaginary")
        self.radioMapImag.toggled.connect(lambda:self.mainWin.on_spectradisplaymode(self.radioMapImag))
        self.radioMapMagnitude.setChecked(True)
        vbox1 = QtWidgets.QVBoxLayout()
        vbox1.addWidget(self.radioMapMagnitude)
        vbox1.addWidget(self.radioMapPhase)
        vbox1.addWidget(self.radioMapReal)
        vbox1.addWidget(self.radioMapImag)
        self.groupboxMapping.setLayout(vbox1)
        
        self.groupboxRange = QtWidgets.QGroupBox(); # 
        self.groupboxRange.setStyleSheet(self.labelStyleSheet);
        self.radioRangeFull = QtWidgets.QRadioButton("Full")
        self.radioRangeFull.toggled.connect(lambda:self.mainWin.on_spectradisplayrange(self.radioRangeFull))
        self.radioRangeHalf = QtWidgets.QRadioButton("Half")
        self.radioRangeHalf.toggled.connect(lambda:self.mainWin.on_spectradisplayrange(self.radioRangeHalf))
        self.radioRangeTenth = QtWidgets.QRadioButton("1/10")
        self.radioRangeTenth.toggled.connect(lambda:self.mainWin.on_spectradisplayrange(self.radioRangeTenth))
        self.radioRangeHundredth = QtWidgets.QRadioButton("1/100")
        self.radioRangeHundredth.toggled.connect(lambda:self.mainWin.on_spectradisplayrange(self.radioRangeHundredth))
        self.radioRangeFull.setChecked(True)
        vbox2 = QtWidgets.QVBoxLayout()
        vbox2.addWidget(self.radioRangeFull)
        vbox2.addWidget(self.radioRangeHalf)
        vbox2.addWidget(self.radioRangeTenth)
        vbox2.addWidget(self.radioRangeHundredth)
        self.groupboxRange.setLayout(vbox2)

        self.rightlayout.addStretch(1)
        self.rightlayout.addWidget(self.groupboxPrecision);
        self.rightlayout.addStretch(1)
        self.rightlayout.addWidget(self.groupboxMapping);
        self.rightlayout.addWidget(self.groupboxRange);
        
        self.rightlayout.addStretch(1)
        
        self.panelRight.setLayout(self.rightlayout)
        
        self.axMain = self.fig.add_subplot(111)
        # self.axMain = plt.subplot(111, projection='3d', proj_type = 'ortho') # fig.subplots()
        # self.fig.subplots_adjust(left=0.12, bottom=0.10, top=0.9, right=0.9)
        self.fig.subplots_adjust(left=0.12, right=0.975, bottom=0.1, top=0.95)
        # self.axMain.set_aspect('equal', 'box')
        
        self.axYLabel = self.axMain.set_ylabel('Transform X[f]')
        self.axXLabel = self.axMain.set_xlabel('f [cycles/1]')
        # self.axZLabel = self.axMain.set_zlabel('Im{ Signal x[n]}')


# class Window(QtWidgets.QDialog):
class Window(QtWidgets.QWidget):
    global styleString, PRINTDEBUG
    # member variables
    winTitle = "SpectraUB";
    winPosition = [500,200,602,640];
    iconName = "pySpectraUB.png";
    axcolor = 'lightgoldenrodyellow'
    footnotesize = 8;
    
    labelStyleSheet = styleString
    
    # member variables
    function_definition_text = 'exp(i*2*pi*n/64)'
    showTransform = 0;
    maxPoints = 8192;
    n = np.arange(0.0, 128.0, 1.0)
    
    isContinuous = False;
    precision = 1e-10;
    precision_text = '1e-10';
    
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        self.DEBUG = PRINTDEBUG;
        self.initLayout();
        self.initBindings();
        self.panelRight.setVisible(False)
        self.on_datadisplaymode(self.panelLeft.radioMapReal)
        self.update();
        self.show();
        
    def update(self):
        # formulaDefinitionStr = '{0}'.format(formulaDefinitionStr.lower())
        isError, signal = self.eval_formula()
        if not isError:
            signal = np.round(signal/self.precision)*self.precision;
            ydata = np.real(signal);
            zdata = np.imag(signal)
            self.hSignal.set_data_3d(self.n, ydata, zdata);
            self.hStem.setData(self.n, ydata, zdata);
            amplitude = np.max([np.max(np.abs(ydata)),np.max(np.abs(zdata))]);
            if isnan(amplitude):
                amplitude = 0
            if not isfinite(amplitude):
                amplitude = 1/np.finfo(float).eps;
            if(amplitude == 0):
                amplitude = 0.02;
            
            # self.hSignal.set_zdata(zdata)
            self.panelLeft.axMain.set_xlim(np.min(self.n), np.max(self.n))
            self.panelLeft.axMain.set_ylim(-amplitude, amplitude)
            self.panelLeft.axMain.set_zlim(-amplitude, amplitude)
#            for t in self.panelLeft.axMain.xaxis.get_major_ticks(): t.label.set_fontsize(8)
#            for t in self.panelLeft.axMain.yaxis.get_major_ticks(): t.label.set_fontsize(8)
#            for t in self.panelLeft.axMain.zaxis.get_major_ticks(): t.label.set_fontsize(8)
#            self.panelLeft.axMain.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
            self.panelLeft.fig.canvas.draw_idle()
            
            self.updateSpectra(signal);
            
    def updateSpectra(self, signal):
        if (self.panelRight.axMain is None):
            return;
         
        dt = self.n[1]-self.n[0];

        nextPowerOfTwo = np.abs(np.max((int(0),np.ceil(np.log2(np.abs(self.n.size))))))
        nextPowerOfTwo = 2.0**nextPowerOfTwo;
        # f        = np.fft.fftfreq(self.n.size, dt)
        f        = scipy.fft.fftfreq(self.n.size, dt)
        transform = scipy.fft.fft(signal)*dt
        self.debugOut("Mapping: ", self.panelRight.mapping)
        if (self.panelRight.mapping == "magnitude"):
            transform = np.abs(transform);
        if (self.panelRight.mapping == "phase"):
            transform = np.angle(transform);
        if (self.panelRight.mapping == "real"):
            transform = np.real(transform);
        if (self.panelRight.mapping == "imaginary"):
            transform = np.imag(transform);            
        transform = scipy.fft.fftshift(transform) / float(self.n.size);
        transform = np.round(transform/self.precision)*self.precision;

        # freqScale  = ((1/max(self.n))/ nextPowerOfTwo);
        freqScale  = ((self.n.size/max(self.n))/ nextPowerOfTwo);
        gfreq      = (np.arange(0,nextPowerOfTwo)-nextPowerOfTwo/2) * freqScale;  # Frequency range

        # f=np.fft.fftshift(f)
        # transform=np.fft.fftshift(transform)
        # f2=np.stack((f,f,np.nan*f)).flatten('F');
        f3 = np.stack((gfreq, gfreq, np.nan*gfreq)).flatten('F')
        transform3 = np.stack((transform, np.array(0.0)*transform,np.nan*transform)).flatten('F')
        
        xlim = np.array([min(gfreq),max(gfreq)])+np.array([-1.0,1.0])*freqScale/2.0;
        self.debugOut("Range: ", self.panelRight.range)
        wScale = 1.;
        if (self.panelRight.range == "Full"):
            pass
        if (self.panelRight.range == "Half"):
            xlim[0] = -freqScale /2.0;
        if (self.panelRight.range == "1/10"):
            # xlim    = np.array([-1.,1.])*np.max(gfreq)/10.0+freqScale/2.0;
            wScale = 5.;
            xlim    = np.array([-1.,1.])*np.abs(gfreq[-1])/10.0;
        if (self.panelRight.range == "1/100"):
            # xlim    = np.array([-1.,1.])*np.max(gfreq)/100.0+freqScale/2.0;
            wScale = 20.;
            xlim    = np.array([-1.,1.])*np.abs(gfreq[-1])/100.0;

        self.hSpectra.set_data(f3,transform3, wScale=wScale)
        # self.hSpectra.set_data(gfreq, transform)

        self.panelRight.axMain.set_xlim(xlim);
    
        # self.panelRight.axMain.get_yaxis().get_offset_text().set_position((0.0,0.95))
        self.panelRight.axMain.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        self.panelRight.fig.canvas.draw_idle()

    def debugOut(self, txt, arg = None):
        if(self.DEBUG):
            if arg is None:
                print(txt)
            else:
                print(txt, arg)

    def keyPressEvent(self, event):
        self.debugOut('key press event fired:', event.key());
        if(event.key()==Qt.Key_Escape):
            self.closeRequest();
        pass;        
        
    def on_editFunctionDefinition(self):
        text = self.editFunctionDefinition.text();
        self.function_definition_text = text.lower()
        self.update()
        
    def on_historywidgetclicked(self, item):
        text = item.text();
        # print('!!! click {}'.format(item.text()))
        self.editFunctionDefinition.setText( text )
        self.function_definition_text = text.lower()
        self.update()
        
    def on_editPrecision(self):
        text = self.panelRight.editPrecision.text();
        self.debugOut(text)
        self.precision = eval(self.precision_text)
        self.precision_text = text
        self.update()

    def eval_formula(self):
        n = self.n  
        self.debugOut(self.function_definition_text)
        i = complex(0,1)
        
        u     = lambda x: (np.array(x)>=0).astype(float)
        step  = lambda x: (np.array(x)>=0).astype(float)
        ε     = lambda x: (np.array(x)>=0).astype(float)
        delta = lambda x: (np.array(x)==0).astype(float)
        δ     = lambda x: (np.array(x)==0).astype(float)
        π     = np.pi
        # mod   = lambda x, y: (np.array(x) % np.array(y)).astype(float)

        # y = eval(self.function_definition_text)
        isError = False;
        y = None;
        try:
            y = eval(self.function_definition_text)
        except:
            ee = sys.exc_info()
            error = f'{ee[1]}'
            isError = True;
            self.labelError.setToolTip(error)
            pass
        self.labelError.setVisible(isError);
        return (isError, y)
    
    def initLayout(self):
        self.setWindowTitle(self.winTitle);
        icon_str = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAKrklEQVR4Ae1aeWxUxx1+QMQpUMRRLnMGbC5jCgJEgCIEpAITRERU9R8ERIDBxl47JolKAsQUjA1NSwhSohaCqjbgCqSkrRrv6RuvucLpQKEGB3PEGDA2Nr75qm+eZ9nr7eHdFbHDk2bf2ze/Ob5vfjPzzbxR8DO/lJ85frwk4KUH2DHQ0tJi969jPjpjdOkCT58+xZMnTzpkIDbny0ZATU0NdDodhgwZgl69etlCz5490Z6DPRZiS0xMBLHKS3n27Jl4JnhFUUQg4B49enSoQEwSH0ngRezCA+rq6jBw4EB069YNOTk54P/a2lpx53N7DxJLbm6uwEis9fX1ggRBAPs8W5wsVVZWtnvAWg1GbNK7iZmXjYA+ffqgU6dOCA8P79CBGInVhYDevXtjzpw5AvzYsWMxbNgwhIWFaYbRo0fbbEeMGKFpxzxCbctG81QHYiEm2pEAYnUhgK4RFRWFMWPGIDIyEsuXL8eSJUuwePFil7B06VJMmjQJgwYNErMGiYuOjnawezN6Cd5aFo23ly/DlKjJwnbw4MGCZKbXynfy5LbZzp49G97yJTbiZHAggNMCx4Bx48Zh1KhRYCUKCwtB0dDc3OwS2HcuXboEo9EIs9mMBw8eiP5EWzxTxVRdYxNuVNSg/EkDrl0pRlGeBRazGRUVFTZb57wZcfnyZZGvyWTC/fv3PdoWFxd7tSWGsrIyTJs2TWDjtEisciq0jQFkZcKECcJN+vbti5SUFFF4W34+L7yLqD2n0PfDfAzYVoDlh68gt6yhLVkFJU1RUZHoouwGbj2A7kBWxo8fLwgYMGAAUlNT/S78YW0jfv35d1DWZ0KJM0LRmaAkmKBs0EOJNSLFcMPvPIORgAQMHz5cjAMkgFgduoAkwN4Dtm/f7lfZtQ3NeP3T01DWZUJ51wIl0Qxlo0ElIskCRWeGsl6P9/593a98g2F84sQJBw/QJIAewDFg6tSpuHDhgl9lr/yq+Dn4OCO6bM7Coi/OYea+01DiTWogKTF6/CHnll95B2p87949zJgxAyNHjtTWAbILcMpYuHChX2V+Yb0tgCls6Y0GROwqxImbj215ZJz7Ea9+kK2SoDOjU4IJ1tIqW3ywH+qaHFe1TU1NWLBggegGHrsAPYB9Zf78+Tap6K1y1ypq0eO9LBVcrAHhOwtx+7EqM+3TGv/7EK/QAzgmxBowIc2K6vome5OgPKdnl2LZl47eS9lLTMQWdAKWHDyvtr7OjJ7JWThzu1oTyKf5ZeqAmKSOB5uDPB4cPVeujjkb9Pj2ijo1szJ+EeBPF/jm8n3RmgoBxeixL69ME7yMWHmkWCVBZ0YXnRmnbgWnK5y/8wTdky2qJ27QY8onJ9HUoq50qTfYBYjNowdwFqBsnThxIrKzs2Wd3d4bmlowPr0ISpxB9Ps5n51B68rarb18WVHTiEHb8qBsMrWmO4sWXxLKDNzcq+ubEZFqVWcddrM4I/6YcwvNrQSUlpYKlUtsHgngGBAREQEKoa1bt7op6vmr/XTnGL2Y7jonmFBoN+g9t3L/9NV3P6qV5aAZowfzCuTaePyqqj2Y3wY9PnbSGwUFBRg6dKjQOB4JYMtzwUAhtGvXLs06UfD8YmselHijAPBOxhVNW62I6IMXVALjTej3YS7Kq9umFL++dN+BzLcOX3Qp0mchJJVgv379sGPHDpeM5Ist35aojOtM6PNBNkof1ckon+9Xy2vRi7NHgjp+rD/mP4n3qhsw/OMCKJuMwu1H7ShARY0rkVzXyBWhRw8gARQL06dPx/Xr7hXbrcp69OacTpm7Xo+PMkt8Bu1suE1/QyUy0YzOOhPySiqdTTz+f/uvl2zpSYLl+iO39lyAzZo1SyyZvRLAuZIjptwrdM5x3bErAjjVHbvBw9q2z+W1jc0I321Vp65YA8btpjZodi7S7f8vT911mIF0/7zm1o4vgyaEONUIMdMqaf+UG7ik/c/3DxyAxBy7qglERpwtqxaaw15UNTQ7qj9py7tfOsCTEhQDF1d2cUbRcmzBYFyrjn6vDogcxeON+NvZe5rZcgCm1BYLLZ0ZXZMsKPrBs5bwmQDqAK0u8I/z5WpLsfU3GnDsQrlmJf2NoDYI42DGJXSCCd3ftSD7f679uaquCfMPnFWFFFec6zOxy1LqtbjGxkbf1gLcEeIgOHPmTNy8edOWMfv5sJTW0XaDAXMPnLXFBevhX8UV6mhOguON6P1+Nv5svY2njaprU2dM++SUA/jov5z3SXw9evRIDIJeV4NyP6B///7YuXOnDduaDLpopip6dCYUhWglt91wo7UrtC6a4owYn2bF6/vP4JXNWaoHUu7G6DFlTxEe1DTa6ujpwWq1+jYNRo6LEEKIBKTtVneEDhSUPR+k1mUi4Wvt0dZTJXyN031zrVVjmNWNFc7xsQZ1g0W4vR4RqYX4odJ1xalVBglg15ZbYpobIpFvLEbE2DEY/GoP7E//Pf5+sUp1S+7mxBowNrXQ52lKqzK+vOfWWZdWgSQ2U1qX0NQdv9p/Brcq/RNePklhCoRhb6zAaxN/iQHzV+K1TQehvF+gbmXFm9AtyRzSTQxnYvJvVOI3hy8iLKUAg7bmYc6+0ziQV2Zb4Tnbe/pfUlLisBgiVoc9Qdu2eHg4wma9CeWjc1CS86EkmlTFt9GAgyfveCojZHFVdc146GNf16oEhRB3uSiHPW6LcxYYOncFlN+dhFjji01NA7iR0Z4vZx3g4gF0B34u4pehgQvfgbLlnNhYiEovAldb7f2yJ0Dz05j8NjgkchbCf7sFOaWupynaMxFyU1STALoFw6jhYZg7PQrWXDMK8nKRlZXlELhTlJ+fj7S0NKxduxYxMTHIyMgQ77Rs09PTvdoyLfPds2ePzfbIkSNu85W2e/fu9WrL8w4Gg8FBCLl0ATkIcoDghgjnS09fhhnHD508aMDAoyee7H8KtvLrsMdBkMyQAEkCE2kFadfebKWnO3wc5R8ZYQ+sIz5LnA4EyG+DdA/OmR31IjbZBRyEEM/UyENSFotFqKSqqipxp2Ggobq6GjJ4y0va8e7N1td4iYXYeBDM4ZCU3PqyPyZHlqSrBOPO/Hguh1Ott/xoxxCKOmgek6PLs08kJSWF5KBk9+7dbWf0tAiQgGUlmUbL1p/3zFcGzlbEyGNz8hInROQf3oN5VPbx48fC9akDOnfujOTkZOHWfO/svtJNV61aJWypM9gN3Nk6p/X1v8ejsgTvfJDYnphAnnkgii1LUeLtOn78uLDlAa1QXM4YXTwgWIXKseXu3bugO/NEmXQ9GWdflnzHPXyOAUxz5466ApVx9vbBeg4ZAeLEGADKWbb+mjVrRJ09gZFxK1asEGmYlpfMK1ig7fMJGQESzOrVqwWYo0ePegUjgR46dMhn0uzBtOU5JARI8HR5uj7dmV2Bl4xzV1nZP/k5u2vXrmK9IRWbp3Tu8vL1XUgIkIVz5Uj3b8uAtmjRIpHW2zkFWVZb70EjwH4qklMXpz1Of5wGfZ3S5NTJadDT1NlWwM7pAiaAX13mzZsnRAuVnhQ0FCuBiBp2G5leCh/mLdUky2TZgV4hJYCVbausJVimlYBJwk+SANkC9l1APgeysPGWVpYb6D1gDwi0Ai86/UsCXnQLvOjyX3rAi26BF13+/wH/u7el1iugaQAAAABJRU5ErkJggg=="
        pm = QtGui.QPixmap()
        pm.loadFromData(base64.b64decode(icon_str))
        # self.setWindowIcon(QtGui.QIcon(self.iconName));
        self.setWindowIcon(QtGui.QIcon(pm));
        self.setGeometry(self.winPosition[0], self.winPosition[1],self.winPosition[2],self.winPosition[3]);

        hbox = QtWidgets.QHBoxLayout()
        hbox.setContentsMargins(0,0,0,0); # (left, top, right, bottom)
        
        self.panelTop = QtWidgets.QFrame()
        self.panelTop.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.panelTop.setFixedHeight(105)
        self.initTopPanelLayout();
        #self.panelTop.fig = Figure()
        #self.panelTop.canvas = FigureCanvas(self.panelTop.fig)
        
        self.panelLeft = GraphPanel(self)
        self.panelLeft.addSignalPanelItems();
        x = np.sin(2 * np.pi * 1/64 * self.n)
        y = np.cos(2 * np.pi * 1/64 * self.n)
        self.hStem = stem3(self.panelLeft.axMain, self.n, y, x)
        self.hSignal, = self.panelLeft.axMain.plot3D(self.n, y, x, lw=1, ls='-.')
        self.hSignal.set_color('#1f77b4'); # '#ff7f0e')
        self.panelLeft.axMain.margins(x=0)
        self.panelLeft.axMain.xaxis.set_tick_params(labelsize=self.footnotesize)
        self.panelLeft.axMain.yaxis.set_tick_params(labelsize=self.footnotesize)
        self.panelLeft.axMain.zaxis.set_tick_params(labelsize=self.footnotesize)
        
        self.panelRight = GraphPanel(self)
        self.panelRight.addSpectraPanelItems();
        x2=np.arange(0,16);
        y2=np.cos(np.pi*x2/8);
        # hb=ax.plot(x2,y2,marker='',ls='-',ms=4,markevery=(0,3))
        # self.hSpectra, = self.panelRight.axMain.plot(x2,y2,marker='',ls='-',ms=4,markevery=(0,3))
        self.hSpectra = spectraPlot(self.panelRight.axMain, x2, y2);
        
        self.splitterTop = QtWidgets.QSplitter(Qt.Vertical)
        self.panelTop.setStyleSheet('background-color:white')
        
        self.splitter = QtWidgets.QSplitter(Qt.Horizontal)
        self.splitter.setHandleWidth(1)
        # add "case_widget" and "log_area" to splitter
        self.splitter.addWidget(self.panelLeft)
        self.splitter.addWidget(self.panelRight)

        # set the initial scale: x1:x2
        x1 = 4; x2 = 1
        # self.splitter.setStretchFactor(0, x1)
        # self.splitter.setStretchFactor(1, x2)
        
        self.splitterTop.addWidget(self.panelTop);
        self.splitterTop.addWidget(self.splitter);
        
        hbox.addWidget(self.splitterTop);
        self.setLayout(hbox);
    
    def initTopPanelLayout(self):
        labelFunction = QtWidgets.QLabel('Definition:')
        labelFunction.setStyleSheet(self.labelStyleSheet);
        self.editFunctionDefinition = QtWidgets.QLineEdit()
        self.editFunctionDefinition.setText(self.function_definition_text);
        self.editFunctionDefinition.setStyleSheet('font: 9pt "Courier";');
        self.editFunctionDefinition.setAlignment(Qt.AlignCenter);
        # https://pythonpyqt.com/pyqt-qlistwidget/
        self.historyList = QtWidgets.QListWidget()
        # self.historyList.setStyleSheet('font: 8pt "Courier";');
        self.historyList.setStyleSheet('font: 8pt "Courier"; QListWidget {padding: 0px;} QListWidget::item { margin: 0px; }')
        self.historyList.addItem('n < 16')
        self.historyList.addItem('cos(2*pi*n/64)')
        self.historyList.addItem('exp(i*2*pi*n/64)')
        self.historyList.addItem('-cos(2*pi*n/16)+sin(2*pi*n/32)')
        
        
        self.labelError = QtWidgets.QLabel('error')
        self.labelError.setAlignment(Qt.AlignCenter);
        self.labelError.setStyleSheet('background-color: OrangeRed; font: 9pt "Courier"; visibility: hidden;');
        # self.labelError.setStyleSheet('background-color: OrangeRed; font: 9pt "Courier"; visibility: hidden;');
        self.labelError.setVisible(False);
        
        length = QtWidgets.QLabel('Length:')
        length.setStyleSheet(self.labelStyleSheet);
        self.cbLength = QtWidgets.QComboBox()
        self.cbLength.setStyleSheet(self.labelStyleSheet);
        self.cbLength.addItems(["16", "32", "64", "128", "256", "512", "1024", "user defined"])
        self.cbLength.setCurrentIndex(3);
        
        self.groupboxSignalType = QtWidgets.QGroupBox(); # 
        self.groupboxSignalType.setStyleSheet(self.labelStyleSheet);
        self.radioTypeDiscrete = QtWidgets.QRadioButton('discrete')
        self.radioTypeDiscrete.setChecked(True)
        self.radioTypeContinuous = QtWidgets.QRadioButton('"continuous"')
        self.radioTypeDiscrete.toggled.connect(lambda:self.on_buttonstate(self.radioTypeDiscrete))
        self.radioTypeContinuous.toggled.connect(lambda:self.on_buttonstate(self.radioTypeContinuous))
        vbox2 = QtWidgets.QVBoxLayout()
        vbox2.addWidget(self.radioTypeDiscrete)
        vbox2.addWidget(self.radioTypeContinuous)
        self.groupboxSignalType.setLayout(vbox2)

        grid = QtWidgets.QGridLayout()
        grid.setSpacing(2)

       #grid.addWidget (QWidget, row, column, rowSpan, columnSpan, Qt.Alignment alignment = 0)
        grid.addWidget(labelFunction, 0, 0)
        grid.addWidget(self.editFunctionDefinition, 0, 1, 1, 4)
        grid.addWidget(self.historyList, 0, 5, 4, 1)

        grid.addWidget(self.labelError, 1, 1, 1, 4)
        
        grid.addWidget(length, 2, 0)
        grid.addWidget(self.cbLength, 2, 1)

        grid.addWidget(self.groupboxSignalType, 2, 3, 1, 2)
        
        # grid.setColumnStretch(0, 0.66)
        # grid.setColumnStretch(1, 1)
        # grid.setColumnStretch(2, 1)
        # grid.setColumnStretch(3, 1.7)
        # grid.setColumnStretch(4, 0.3)
        # grid.setColumnStretch(5, 2.1)
        grid.setColumnStretch(0, 1)
        grid.setColumnStretch(1, 3)
        grid.setColumnStretch(2, 3)
        grid.setColumnStretch(3, 5)
        grid.setColumnStretch(4, 1)
        grid.setColumnStretch(5, 6)
        
        self.panelTop.setLayout(grid) 
        
    def initBindings(self):
        # self.actionExit.triggered.connect(self.close);
        self.cbLength.currentIndexChanged.connect(self.on_cbLength);
        self.editFunctionDefinition.returnPressed.connect(self.on_editFunctionDefinition)
        self.historyList.itemClicked.connect(self.on_historywidgetclicked)
        self.panelRight.editPrecision.returnPressed.connect(self.on_editPrecision)
        # self.panelLeft.checkBoxShowTransform.stateChanged.connect(lambda:self.on_buttonstate(self.panelLeft.checkBoxShowTransform))
        c1 = self.panelLeft.canvas.mpl_connect('motion_notify_event', self.on_move)
        
        self.on_buttonstate(self.panelLeft.checkBoxShowOptions)
        app = QtWidgets.QApplication.instance()
        # app.aboutToQuit.connect(self.closeEvent)
        pass;
        
    def on_move(self, event):
        el = self.panelLeft.axMain.elev;
        az = self.panelLeft.axMain.azim;
        di = 10; # self.panelLeft.axMain.dist;
        # print("Elev:",el,", azim:", az);
        # diVal = 7.1+1.8462*np.arctan(np.max([250-500*np.abs(0.5-np.abs(np.cos(pi*el/180))),250-500*np.abs(0.5-np.cos(pi*az/180) ** 2)]));
        diVal = 7.1+1.8462*np.arctan(np.linalg.norm([50-100*np.abs(0.5-np.abs(np.cos(pi*el/180))),50-100*np.abs(0.5-np.abs(np.cos(pi*el/180)))]));
        diVal = 10.0/diVal; # 1.4
        xlabel = 'n';
        ylabel = 'Re{ Signal x[n]}';
        zlabel = 'Im{ Signal x[n]}';
        self.panelLeft.axMain.xaxis.set_major_locator(AutoLocator())  # solution
        self.panelLeft.axMain.yaxis.set_major_locator(AutoLocator())  # solution
        self.panelLeft.axMain.zaxis.set_major_locator(AutoLocator())  # solution
        if (np.abs(el)<6) and (np.abs(-90-az)<6):
            self.panelLeft.axMain.set_yticks([]); ylabel='';
            di = 7.1;
        if (np.abs(90-el)<6) and (np.abs(-90-az)<6):
            self.panelLeft.axMain.set_zticks([]); zlabel='';
            di = 7.1;
        if (np.abs(el)<6) and (np.abs(az)<6):
            self.panelLeft.axMain.set_xticks([]); xlabel='';
            di = 7.1;
        if (np.abs(el)<6) and (np.abs(180-np.abs(az))<6):
            self.panelLeft.axMain.set_xticks([]); xlabel='';
            di = 7.1;
        if (np.abs(180-np.abs(el))<6) and (np.abs(180-np.abs(az))<6):
            self.panelLeft.axMain.set_xticks([]); xlabel='';
            di = 7.1;
        self.panelLeft.axYLabel = self.panelLeft.axMain.set_ylabel(ylabel);
        self.panelLeft.axXLabel = self.panelLeft.axMain.set_xlabel(xlabel);
        self.panelLeft.axZLabel = self.panelLeft.axMain.set_zlabel(zlabel);
        # self.panelLeft.axMain.dist = diVal;
        self.panelLeft.axMain.set_box_aspect(None, zoom=diVal)
        pass;
        
    # def btnstate(self,b):
    def on_buttonstate(self,b):
        if(self.panelLeft.axMain is None):
            return;
        if b.isChecked() == True:
            self.debugOut(b.text()+" is selected")
        else:
            self.debugOut(b.text()+" is deselected")
				
        if (b.isChecked() == True) and (b.text() == 'discrete'):
            self.isContinuous = False;
#            self.hStem.show_marker(True);
#            self.hStem.show_line(True);
            self.hSignal.set_linestyle('-.');
            self.panelLeft.checkBoxStyleMarker.setChecked(True)
            self.panelLeft.checkBoxStyleConnect.setChecked(True)
            self.panelLeft.checkBoxStyleStem.setChecked(True)
            self.on_cbLength(-1);
        if (b.isChecked() == True) and (b.text() == '"continuous"'):
            self.isContinuous = True;
#            self.hStem.show_marker(False);
#            self.hStem.show_line(False);
            self.hSignal.set_linestyle('-');
            self.panelLeft.checkBoxStyleMarker.setChecked(False)
            self.panelLeft.checkBoxStyleConnect.setChecked(True)
            self.panelLeft.checkBoxStyleStem.setChecked(False)
            self.on_cbLength(-1);
        if b.text() == "Options":
            bSetVisible = b.isChecked();
            self.panelLeft.groupboxProject.setVisible(bSetVisible);
            self.panelLeft.groupboxStyle.setVisible(bSetVisible);
            # XYZ XXX Toggle Below: Show FFT Fourier Transform
            self.panelLeft.checkBoxShowTransform.setVisible(bSetVisible);
        if (b.isChecked() == True) and (self.panelLeft.axMain is not None):
            if b.text() == "perspective":
                self.panelLeft.axMain.set_proj_type('persp');
            if b.text() == "orthographic":
                self.panelLeft.axMain.set_proj_type('ortho');
            self.panelLeft.fig.canvas.draw_idle();
        if b.text() == "Marker":
            bSetVisible = b.isChecked();
            self.hStem.show_marker(bSetVisible);
        if (b.isChecked() == True) and (b.text() == "o Marker"):
            self.hStem.set_marker('o');
        if (b.isChecked() == True) and (b.text() == "* Marker"):
            self.hStem.set_marker('*');
        if b.text() == "connect":
            bSetVisible = b.isChecked();
            if(bSetVisible):
                if(self.isContinuous):
                    self.hSignal.set_linestyle('-');
                else:
                    self.hSignal.set_linestyle('-.');
            else:
                self.hSignal.set_linestyle('');
        if b.text() == "stem":
            bSetVisible = b.isChecked();
            self.hStem.show_line(bSetVisible);
        if b.text() == "Transform":
            bSetVisible = b.isChecked();
            self.panelRight.setVisible(bSetVisible)
            app = QtWidgets.QApplication.instance()
            screen_resolution = app.desktop().screenGeometry()
            screenWidth, screenHeight = screen_resolution.width(), screen_resolution.height()
            winSize = self.size();
            newWidth, newHeight = winSize.width(), winSize.height();
            if(bSetVisible):
                newWidth = newWidth*2;
                newWidth = int(np.min([newWidth,screenWidth]));
            else:
                newWidth = newWidth/2;
                newWidth = int(np.max([newWidth,self.winPosition[3]/2]));
            self.resize(newWidth, newHeight)
        self.panelLeft.fig.canvas.draw_idle();
            
            
    def on_datadisplaymode(self,b):
        self.debugOut('datadisplaymode changed.');
        if (b.isChecked() == True) and (self.panelLeft.axMain is not None):
            E = 0.00001;
            # plot3d:  x-axis=n,     y-axis=real,    z-axis=imag
            xlabel = 'n';
            ylabel = 'Re{ Signal x[n]}';
            zlabel = 'Im{ Signal x[n]}';
            if b.text() == "real":
                self.panelLeft.axMain.view_init(90.0-E, -90.0-E);
                self.on_move(None);
#                self.panelLeft.axMain.set_zticks([]); zlabel='';
#                self.panelLeft.axMain.xaxis.set_major_locator(AutoLocator())  # solution
#                self.panelLeft.axMain.yaxis.set_major_locator(AutoLocator())  # solution
            if b.text() == "imaginary":
                self.panelLeft.axMain.view_init(0, -90.0-E);
                self.on_move(None);
#                self.panelLeft.axMain.set_yticks([]); ylabel='';
#                self.panelLeft.axMain.xaxis.set_major_locator(AutoLocator())  # solution
#                self.panelLeft.axMain.zaxis.set_major_locator(AutoLocator())  # solution
            if b.text() == "3d":
                self.panelLeft.axMain.view_init(30,-60);
                self.on_move(None);
#                self.panelLeft.axMain.xaxis.set_major_locator(AutoLocator())  # solution
#                self.panelLeft.axMain.yaxis.set_major_locator(AutoLocator())  # solution
#                self.panelLeft.axMain.zaxis.set_major_locator(AutoLocator())  # solution
            if b.text() == "project":
                self.panelLeft.axMain.view_init(0, 0); # -90, 0);
                self.on_move(None);
#                self.panelLeft.axMain.set_xticks([]); xlabel='';
#                self.panelLeft.axMain.yaxis.set_major_locator(AutoLocator())  # solution
#                self.panelLeft.axMain.zaxis.set_major_locator(AutoLocator())  # solution
#            self.panelLeft.axYLabel = self.panelLeft.axMain.set_ylabel(ylabel);
#            self.panelLeft.axXLabel = self.panelLeft.axMain.set_xlabel(xlabel);
#            self.panelLeft.axZLabel = self.panelLeft.axMain.set_zlabel(zlabel);
            self.panelLeft.fig.canvas.draw_idle();
            
    def on_spectradisplaymode(self,b):
        self.debugOut('spectradisplaymode changed.');
        if (self.panelRight.axMain is None):
            return;
        if (b.isChecked() == True) and (self.panelRight.axMain is not None):
            self.debugOut('Button: ', b.text());
            self.panelRight.mapping = b.text();
            self.update();
            # self.panelRight.fig.canvas.draw_idle();
            # if b.text() == "magnitude":
            #     pass;
            # if b.text() == "phase":
            #     pass;
            # if b.text() == "real":
            #     pass;
            # if b.text() == "imaginary":
            #     pass;

    def on_spectradisplayrange(self,b):
        self.debugOut('spectradisplayrange changed.');
        if (self.panelRight.axMain is None):
            return;
        if (b.isChecked() == True) and (self.panelRight.axMain is not None):
            self.debugOut('Button: ', b.text());
            self.panelRight.range = b.text();
            self.update();
        # if b.text() == "Full":
        #     pass
        #     # bSetVisible = b.isChecked();
        #     # self.hStem.show_line(bSetVisible);
        # if b.text() == "Half":
        #     pass
        # if b.text() == "1/10":
        #     pass
        # if b.text() == "1/100":
        #     pass
                
        
    def on_cbLength(self, i):
        #for count in range(self.cb.count()):
        #   print self.cb.itemText(count)
        self.debugOut("Current index: ",i)
        self.debugOut("selection changed ",self.cbLength.currentText())
        newValue = 0;
        try:
            newValue = float(int(self.cbLength.currentText()));
        except:
            pass
        if(newValue > 0):
            if(self.isContinuous):
                self.n = np.arange(0.0, 8192.0)/8192.0*newValue;
                # self.n = np.arange(0.0, newValue, 1.0);
            else:
                self.n = np.arange(0.0, newValue, 1.0);
            self.update();
        
        
    def closeEvent(self, event):
        self.debugOut('Closing')
        # app = QtWidgets.QApplication.instance()
        # app.closeAllWindows()
        # app.closeAllWindows()
        # app.quit()
        QtWidgets.QApplication.quit()
        event.accept()
            
    def closeRequest(self):
        sys.stderr.write('\r')
        if QtWidgets.QMessageBox.question(None, '', "Do you want to quit SpectraUB?",
                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                QtWidgets.QMessageBox.No) == QtWidgets.QMessageBox.Yes:
            self.close()
            QtWidgets.QApplication.quit()
        

    def sigint_handler(self, *args):
        """Handler for the SIGINT signal."""
        self.closeRequest();
    
def mainProc():
    app = QtWidgets.QApplication.instance()
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    app.lastWindowClosed.connect(app.quit)

    main = Window()
    signal.signal(signal.SIGINT, main.sigint_handler); # signal.SIG_DFL)

    timer = QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(100)
    
    app.exec_()
    del app
    
if __name__ == '__main__':
    mainProc();
#    import threading
#    t = threading.Thread(target=mainProc)
#    t.daemon = True
#    t.start()

    